package ru.magmel.infrastructure.persistence.repository.imported

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import ru.magmel.infrastructure.persistence.model.LoanApplication
import java.util.*

@Repository
interface ImportedLoanApplicationJpaRepository : JpaRepository<LoanApplication, UUID>
