package ru.magmel.infrastructure.persistence.model

enum class ClientType {
    BORROWER, PLEDGER
}