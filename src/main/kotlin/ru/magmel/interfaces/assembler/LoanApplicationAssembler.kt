package ru.magmel.interfaces.assembler

import ru.magmel.domain.model.LoanApplication
import ru.magmel.interfaces.dto.get.GetLoanApplicationResponse
import ru.magmel.interfaces.dto.post.PostLoanApplicationRequest
import java.util.UUID

object LoanApplicationAssembler {
    fun toDomain(postLoanApplicationRequest: PostLoanApplicationRequest): LoanApplication =
        LoanApplication(
            id = UUID.randomUUID(),
            borrowers = postLoanApplicationRequest.borrowers.map(BorrowerAssembler::toDomain).toMutableList()
        )

    fun toDto(loanApplication: LoanApplication): GetLoanApplicationResponse =
        GetLoanApplicationResponse(
            id = loanApplication.id,
            borrowers = loanApplication.getBorrowers().map(BorrowerAssembler::toDto),
            comments = loanApplication.getComments().map(CommentAssembler::toDto)
        )
}