package ru.magmel.infrastructure.framework.mq

import org.w3c.dom.Document
import org.w3c.dom.Node
import org.xml.sax.InputSource
import java.io.ByteArrayOutputStream
import java.io.StringReader
import java.io.StringWriter
import javax.xml.bind.JAXBContext
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.soap.MessageFactory
import javax.xml.soap.SOAPMessage
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

abstract class AbstractJmsSender {

    inline fun <reified T> parseSoapMessage(message: Any): String {
        return MessageFactory
            .newInstance()
            .createMessage()
            .marshal(message, T::class.java)
            .toDocument()
            .removeEmptyNodes()
            .parseToString()
    }

    fun SOAPMessage.marshal(message: Any, classForMapping: Class<*>): SOAPMessage {
        JAXBContext
            .newInstance(classForMapping)
            .createMarshaller()
            .marshal(message, this.soapBody)
        return this
    }

    fun SOAPMessage.toDocument(): Document {
        ByteArrayOutputStream().use { stream ->
            this.writeTo(stream)
            return DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(InputSource(StringReader(String(stream.toByteArray()))))

        }
    }

    fun Node.removeEmptyNodes(): Node {
        val nodeList = this.childNodes
        var index = 0
        while (index < nodeList.length) {
            val childNode = nodeList.item(index)
            if (childNode.textContent.isEmpty()) {
                childNode.parentNode.removeChild(childNode)
                index--
            }
            childNode.removeEmptyNodes()
            index++
        }
        return this
    }

    fun Node.parseToString(): String {
        return StringWriter().also { stringWriter ->
            TransformerFactory
                .newInstance()
                .newTransformer()
                .transform(DOMSource(this), StreamResult(stringWriter))

        }.toString()
    }
}