package ru.magmel.framework.config

import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration
import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication
import org.camunda.bpm.spring.boot.starter.configuration.CamundaDatasourceConfiguration
import org.camunda.bpm.spring.boot.starter.configuration.impl.DefaultDatasourceConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.jdbc.datasource.DataSourceTransactionManager

@EnableProcessApplication
class CamundaConfig {

    @Bean
    fun camundaDatasourceConfiguration(): CamundaDatasourceConfiguration {
        return object : DefaultDatasourceConfiguration() {
            override fun preInit(processEngineConfiguration: SpringProcessEngineConfiguration) {
                super.preInit(processEngineConfiguration)
                processEngineConfiguration.transactionManager = DataSourceTransactionManager(camundaDataSource)
            }
        }
    }
}