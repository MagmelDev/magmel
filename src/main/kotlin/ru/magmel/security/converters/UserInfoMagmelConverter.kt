package ru.magmel.security.converters

import org.json.JSONObject
import ru.magmel.security.Constants
import ru.magmel.security.authentication.MagmelRole
import ru.magmel.security.authentication.UserInfoMagmel
import ru.magmel.security.exceptions.AccessDeniedByResourceAccessByAudienceNotFoundException
import ru.magmel.security.exceptions.AccessDeniedByResourceAccessNotFoundException
import ru.magmel.security.exceptions.NoRoleException

class UserInfoMagmelConverter : UserInfoConverter<UserInfoMagmel> {

    private val audience = Constants.AUDIENCE_MAGMEL

    override fun canApplied(userInfo: JSONObject) = UserInfoConverterUtils.canAppliedByAudience(userInfo, audience)

    override fun decodeJson(userInfo: JSONObject): UserInfoMagmel {
        val userName = UserInfoConverterUtils.getUserName(userInfo)
        val clientId = UserInfoConverterUtils.getClientId(userInfo)
        val resourceAccess =
            userInfo.optJSONObject("resource_access") ?: throw AccessDeniedByResourceAccessNotFoundException()
        val resource =
            resourceAccess.optJSONObject(audience) ?: throw AccessDeniedByResourceAccessByAudienceNotFoundException(
                audience
            )
        return UserInfoMagmel(
            userName = userName,
            clientId = clientId,
            audience = audience
        ).apply {
            roles = getRequiredRolesOrThrow(resource)
        }
    }

    private fun getRequiredRolesOrThrow(resource: JSONObject): List<MagmelRole> {
        val roles = resource.optJSONArray("roles")
        if (roles == null || roles.isEmpty) {
            throw NoRoleException()
        }
        return roles.map { role -> MagmelRole.parse(role.toString()) }
    }
}
