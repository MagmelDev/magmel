package ru.magmel.domain

import ru.magmel.domain.model.Name
import spock.lang.Specification
import spock.lang.Unroll

class NameMust extends Specification {

    def "not throw exception"() {
        when:
        def name = new Name(lastName, firstName, middleName)

        then:
        noExceptionThrown()
        name != null

        where:
        lastName     | firstName  | middleName
        "Маяковский" | "Владимир" | "Владимирович"
        "Маяковский" | "Владимир" | null
    }

    def "ignore middleName=null on fullName"() {
        when:
        def name = new Name("Маяковский", "Владимир", middleName)

        then:
        noExceptionThrown()
        name.fullName == fullName

        where:
        middleName     | fullName
        "Владимирович" | "Маяковский Владимир Владимирович"
        null           | "Маяковский Владимир"
    }

    @Unroll
    def "throw IllegalStateException with message = #message when name = '#lastName #firstName #middleName'"() {
        when:
        new Name(lastName, firstName, middleName)

        then:
        def ex = thrown(IllegalStateException)
        ex.message == message

        where:
        lastName     | firstName  | middleName      || message
        ""           | "Владимир" | "Владимирович"  || Name.EMPTY_LAST_NAME
        "Маяковский" | ""         | "Владимирович"  || Name.EMPTY_FIRST_NAME
        "Маяковский" | "Владимир" | ""              || Name.EMPTY_MIDDLE_NAME

        "          " | "Владимир" | "Владимирович"  || Name.EMPTY_LAST_NAME
        "Маяковский" | "        " | "Владимирович"  || Name.EMPTY_FIRST_NAME
        "Маяковский" | "Владимир" | "            "  || Name.EMPTY_MIDDLE_NAME

        "Mayakovsky" | "Владимир" | "Владимирович"  || Name.INCORRECT_LAST_NAME
        "Маяковский" | "Vladimir" | "Владимирович"  || Name.INCORRECT_FIRST_NAME
        "Маяковский" | "Владимир" | "Vladimirovich" || Name.INCORRECT_MIDDLE_NAME
    }
}
