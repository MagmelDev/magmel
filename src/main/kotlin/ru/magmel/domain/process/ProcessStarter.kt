package ru.magmel.domain.process

interface ProcessStarter {
    fun startMainProcess(businessKey: String, variables: Map<String, Any> = emptyMap()): String
}