package ru.magmel.application

import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.domain.DigitalProfileSender
import ru.magmel.domain.model.LoanApplication
import ru.magmel.framework.ThreadPoolTaskExecutor.Companion.MAGMEL_THREAD
import java.util.UUID

@Service
@Transactional
class DigitalProfileService(
    private val loanApplicationService: LoanApplicationService,
    private val digitalProfileSender: DigitalProfileSender
) {

    private val log = LoggerFactory.getLogger(javaClass)

    @Async(MAGMEL_THREAD)
    fun send(id: UUID): LoanApplication {
        return loanApplicationService.getReadOnly(id).apply {
            digitalProfileSender.send(this)
        }
    }
}