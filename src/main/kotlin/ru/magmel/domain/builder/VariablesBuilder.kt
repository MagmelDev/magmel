package ru.magmel.domain.builder

import ru.magmel.domain.model.Client
import ru.magmel.domain.model.RiskDecision
import ru.magmel.domain.process.Variables.Companion.IDENTIFIERS
import ru.magmel.domain.process.Variables.Companion.RISK_DECISION

class VariablesBuilder {

    private val variables = mutableMapOf<String, Any?>()

    fun identifiers(clients: List<Client>) = this.apply {
        variables[IDENTIFIERS] = clients.mapNotNull { it.clientBankData?.identifier }
    }

    fun riskDecision(riskDecision: RiskDecision) = this.apply {
        variables[RISK_DECISION] = riskDecision.name
    }

    fun please() = variables
}