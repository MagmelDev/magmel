package ru.magmel.framework.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import ru.magmel.framework.ThreadPoolTaskExecutor.Companion.MAGMEL_THREAD
import java.util.concurrent.RejectedExecutionHandler
import java.util.concurrent.ThreadPoolExecutor

@Configuration
class ThreadPoolTaskExecutorConfig {

    @Bean(MAGMEL_THREAD)
    fun threadPoolTaskExecutor(): ThreadPoolTaskExecutor {
        return ThreadPoolTaskExecutor().apply {
            corePoolSize = 2
            maxPoolSize = 2
            setQueueCapacity(2)
            setRejectedExecutionHandler(ExternalTaskRejectedExecutionHandler())
            setThreadNamePrefix("magmel")
            initialize()
        }
    }

    class ExternalTaskRejectedExecutionHandler : RejectedExecutionHandler {
        override fun rejectedExecution(r: Runnable?, executor: ThreadPoolExecutor?) {
            /**
             * Nothing to do here, we are playing around Camunda external tasks
             */
        }
    }
}