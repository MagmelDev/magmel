package ru.magmel.interfaces.assembler

import ru.magmel.domain.model.Borrower
import ru.magmel.interfaces.dto.get.GetBorrowerDto
import ru.magmel.interfaces.dto.post.PostBorrowerDto

object BorrowerAssembler {
    fun toDomain(borrowerResource: PostBorrowerDto): Borrower =
        Borrower(
            name = NameAssembler.toDomain(borrowerResource.name)
        )

    fun toDto(borrower: Borrower): GetBorrowerDto =
        GetBorrowerDto(
            name = NameAssembler.toDto(borrower.name)
        )
}