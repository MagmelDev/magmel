package ru.magmel.infrastructure.persistence.mapper

import ru.magmel.domain.model.Comment as DomainComment
import ru.magmel.infrastructure.persistence.model.Comment

object CommentMapper {

    fun toPersistence(domainComment: DomainComment): Comment {
        return Comment(
            id = domainComment.id,
            text = domainComment.text
        )
    }

    fun toDomain(comment: Comment): DomainComment {
        return DomainComment(
            id = comment.id,
            text = comment.text,
        )
    }
}