package ru.magmel.domain.process

interface PartnerPortalProcessor {
    fun process(businessKey: String, variables: Map<String, Any> = emptyMap())
}