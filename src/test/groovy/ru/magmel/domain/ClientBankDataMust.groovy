package ru.magmel.domain


import ru.magmel.domain.model.ClientBankData
import spock.lang.Specification
import spock.lang.Unroll

class ClientBankDataMust extends Specification {

    @Unroll
    def "not throw exception when identifier=#identifier is valid"() {
        when:
        def clientBankData = new ClientBankData(UUID.randomUUID(), identifier)

        then:
        noExceptionThrown()
        clientBankData != null

        where:
        identifier << [null, "identifier"]
    }

    @Unroll
    def "throw exception when identifier=#identifier"() {
        when:
        new ClientBankData(UUID.randomUUID(), identifier)

        then:
        def ex = thrown(IllegalStateException)
        ex.message == ClientBankData.EMPTY_IDENTIFIER

        where:
        identifier << ["", " "]
    }
}
