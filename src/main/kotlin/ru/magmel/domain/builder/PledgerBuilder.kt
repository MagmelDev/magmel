package ru.magmel.domain.builder

import ru.magmel.domain.model.Pledger

class PledgerBuilder {

    private val pledger = Pledger(
        name = NameHelper.mikhailBulgakov()
    )

    fun withChanges(buildAction: (Pledger) -> Unit) = this.apply {
        buildAction(pledger)
    }

    fun please() = pledger
}