package ru.magmel.domain

import ru.magmel.domain.model.LoanApplication

interface PartnerPortalSender {
    fun send(loanApplication: LoanApplication): String
}