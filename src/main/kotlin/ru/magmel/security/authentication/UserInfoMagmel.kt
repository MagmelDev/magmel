package ru.magmel.security.authentication

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.SpringSecurityCoreVersion
import org.springframework.security.core.authority.SimpleGrantedAuthority
import ru.magmel.security.Constants
import ru.magmel.security.exceptions.UnsupportedRoleException
import java.security.Principal

/**
 * Пользователь MAGMEL
 * @see ru.magmel.security.converters.UserInfoMagmelConverter
 * @see ru.magmel.security.config.annotation.AuthorizeMagmel
 * @see ru.magmel.security.config.annotation.AuthorizeMagmelPersonalMortgageManager
 * @see ru.magmel.security.config.annotation.AuthorizeMagmelCreditAdministrator
 * @see ru.magmel.security.config.annotation.AuthorizeMagmelDealManager
 */
data class UserInfoMagmel(
    val userName: String,
    val clientId: String,
    val audience: String
) : Authentication, Principal {

    lateinit var roles: List<MagmelRole>

    companion object {
        @JvmStatic
        private val serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID
    }

    override fun getName(): String {
        return "userName:$userName; clientId:$clientId"
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
        roles.map { role ->
            SimpleGrantedAuthority(Constants.ROLE_PREFIX + role.value)
        }.plus(SimpleGrantedAuthority(Constants.AUD_PREFIX + audience)).toMutableSet()

    override fun getCredentials(): Any {
        return this
    }

    override fun getDetails(): Any {
        return this
    }

    override fun getPrincipal(): Principal {
        return this
    }

    override fun isAuthenticated(): Boolean {
        return true
    }

    override fun setAuthenticated(isAuthenticated: Boolean) {}
}

enum class MagmelRole(val value: String) {
    PersonalMortgageManager(MagmelRole.PERSONAL_MORTGAGE_MANAGER),
    CreditAdministrator(MagmelRole.CREDIT_ADMINISTRATOR),
    DealManager(MagmelRole.DEAL_MANAGER);

    companion object {
        const val PERSONAL_MORTGAGE_MANAGER = "pmm"
        const val CREDIT_ADMINISTRATOR = "ca"
        const val DEAL_MANAGER = "dm"
        fun parse(name: String): MagmelRole {
            return values().firstOrNull {
                it.value == name
            } ?: throw UnsupportedRoleException(name)
        }
    }
}
