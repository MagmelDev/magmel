package ru.magmel.infrastructure.persistence.model

import ru.magmel.infrastructure.persistence.Schema
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "LOAN_APPLICATION", schema = Schema.CORE)
data class LoanApplication(
    @Id
    val id: UUID,

    @OneToMany(cascade = [(CascadeType.ALL)], fetch = FetchType.LAZY)
    var clients: MutableList<Client> = mutableListOf(),

    @OneToMany(cascade = [(CascadeType.ALL)], fetch = FetchType.LAZY)
    var comments: MutableList<Comment> = mutableListOf(),

    @Enumerated(EnumType.STRING)
    var riskDecision: RiskDecision?
)
