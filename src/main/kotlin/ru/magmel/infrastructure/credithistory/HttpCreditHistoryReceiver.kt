package ru.magmel.infrastructure.credithistory

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import ru.magmel.domain.CreditHistoryReceiver
import ru.magmel.util.withException
import ru.magmel.infrastructure.credithistory.model.Response

@Component
class HttpCreditHistoryReceiver(
    private val restTemplate: RestTemplate,
    @Value("\${integration.credithistory.host}") private val host: String
) : CreditHistoryReceiver {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun get(identifier: String): Result<String?> {
        return runCatching {
            val response = restTemplate.getForEntity(
                "$host/client/$identifier",
                Response::class.java
            )
            response.body!!.client.status
        }.onSuccess { status ->
            log.info("Success! Found status=$status")
        }.onFailure { ex ->
            log.error("Unexpected error {}", withException(ex))
        }
    }
}