package ru.magmel.infrastructure.camunda.framework

import org.camunda.bpm.engine.externaltask.LockedExternalTask
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withElapsedTime
import ru.magmel.util.withErrorDetails
import ru.magmel.util.withErrorMessage
import kotlin.system.measureTimeMillis

abstract class NoncriticalWorker(
    workerId: String,
    topicName: String,
) : Worker(workerId, topicName) {

    abstract val initRetries: Int

    abstract val retryTimeout: Long

    fun handleNoncriticalFailure(
        lockedExternalTask: LockedExternalTask,
        errorMessage: String,
        errorDetails: String,
        variables: Map<String, Any?> = mapOf()
    ) {
        val leftRetries = (lockedExternalTask.retries ?: initRetries) - 1
        if (leftRetries < 1) {
            complete(lockedExternalTask, variables)
        } else {
            measureTimeMillis {
                externalTaskService.handleFailure(
                    lockedExternalTask.id,
                    workerId,
                    errorMessage,
                    errorDetails,
                    leftRetries,
                    retryTimeout
                )
            }.let { duration ->
                log.info(
                    "Handle noncritical failure {} with {} {} in {}. Retries left {}",
                    withBusinessKey(lockedExternalTask.businessKey),
                    withErrorMessage(errorMessage),
                    withErrorDetails(errorDetails),
                    withElapsedTime(duration),
                    leftRetries
                )
            }
        }
    }
}