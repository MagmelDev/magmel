package ru.magmel.infrastructure.persistence.model

import ru.magmel.infrastructure.persistence.Schema
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "CLIENT_BANK_DATA", schema = Schema.CORE)
data class ClientBankData(
    @Id
    val id: UUID,
    val identifier: String? = null
)