package ru.magmel.util

import com.google.common.base.Throwables
import net.logstash.logback.argument.StructuredArgument
import net.logstash.logback.argument.StructuredArguments.keyValue
import java.util.*

fun withBusinessKey(businessKey: String): StructuredArgument =
    keyValue("businessKey", businessKey)

fun withBusinessKey(businessKey: UUID): StructuredArgument =
    keyValue("businessKey", businessKey)

fun withId(id: UUID): StructuredArgument =
    keyValue("id", id)

fun withJmsMessageId(jmsMessageId: String?): StructuredArgument =
    keyValue("jmsMessageId", jmsMessageId)

fun withProcessInstanceId(processInstanceId: String): StructuredArgument =
    keyValue("processInstanceId", processInstanceId)

fun withElapsedTime(elapsedTime: Long): StructuredArgument =
    keyValue("elapsedTime", elapsedTime)

fun withException(throwable: Throwable?): StructuredArgument =
    keyValue("exception", throwable?.let(Throwables::getStackTraceAsString))

fun withErrorMessage(errorMessage: String): StructuredArgument =
    keyValue("errorMessage", errorMessage)

fun withErrorDetails(errorDetails: String): StructuredArgument =
    keyValue("errorDetails", errorDetails)

fun withJson(json: String): StructuredArgument =
    keyValue("json", "\n$json")

fun withVariables(variables: Map<String, Any?>): StructuredArgument =
    keyValue("variables", variables)

fun withErrorCode(errorCode: String): StructuredArgument =
    keyValue("errorCode", errorCode)

fun withIdentifier(identifier: String): StructuredArgument =
    keyValue("identifier", identifier)