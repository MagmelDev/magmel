package ru.magmel.infrastructure.persistence.model

import javax.persistence.Embeddable

@Embeddable
data class Name(
    val lastName: String,
    val firstName: String,
    val middleName: String?
)