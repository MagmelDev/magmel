package ru.magmel.infrastructure.partnerportal.mq

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.jms.annotation.JmsListener
import org.springframework.jms.core.JmsTemplate
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import ru.magmel.framework.SpringProfile
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withException
import java.nio.charset.Charset
import javax.jms.Message
import javax.jms.TextMessage

@Component
@Profile(value = [SpringProfile.LOCAL])
class PartnerPortalStub(
    private val jmsTemplate: JmsTemplate,
    @Value("\${integration.partnerPortal.responseQueue}") private val responseQueue: String
) {

    private val log = LoggerFactory.getLogger(javaClass)

    init {
        log.warn("${this.javaClass.name} stub is active!")
    }

    @JmsListener(
        destination = "\${integration.partnerPortal.requestQueue}",
        containerFactory = "jmsListenerContainerFactory"
    )
    fun onMessage(@Payload message: Message) {
        log.info("On message fire")
        runCatching {
            Pair(message.jmsMessageID!!, process(message as TextMessage))
        }.onSuccess { (jmsMessageId, response) ->
            jmsTemplate.convertAndSend(responseQueue, response) {
                it.jmsCorrelationID = jmsMessageId
                it
            }
        }.onFailure { ex ->
            log.error("Unexpected error {}", withException(ex))
        }
    }

    private fun process(message: TextMessage): String {
        val id = message.text.substringAfter("<id>").substringBefore("</id>").trim()
        log.info("Processing {}", withBusinessKey(id))
        return resource("/stub/partnerportal/default-response.xml")
            .replace("%id%", id)
    }

    private fun resource(fileName: String) = this.javaClass.getResource(fileName).readText(Charset.forName("UTF-8"))
}