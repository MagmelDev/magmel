package ru.magmel.infrastructure.persistence.model

import ru.magmel.infrastructure.persistence.Schema
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "COMMENT", schema = Schema.CORE)
data class Comment(
    @Id
    val id: UUID,

    val text: String
)