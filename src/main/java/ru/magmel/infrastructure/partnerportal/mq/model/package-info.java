@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(value = LocalDateXmlAdapter.class, type = LocalDate.class),
        @XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class, type = LocalDateTime.class),
        @XmlJavaTypeAdapter(value = ZonedDateTimeXmlAdapter.class, type = ZonedDateTime.class)
})
package ru.magmel.infrastructure.partnerportal.mq.model;

import com.migesok.jaxb.adapter.javatime.LocalDateTimeXmlAdapter;
import com.migesok.jaxb.adapter.javatime.LocalDateXmlAdapter;
import com.migesok.jaxb.adapter.javatime.ZonedDateTimeXmlAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;