package ru.magmel.infrastructure.riskcalculator.model

data class Response(
    val riskDecision: String
)