package ru.magmel.security.config

import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.context.SecurityContextRepository
import java.util.function.Supplier
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CustomRequestAttributeSecurityContextRepository : SecurityContextRepository {

    private val requestAttributeName = CustomRequestAttributeSecurityContextRepository::class.java.name + ".SPRING_SECURITY_CONTEXT"

    override fun containsContext(request: HttpServletRequest): Boolean {
        return loadContext(request).get() != null
    }

    @Suppress("DEPRECATION")
    @Deprecated(message = "Deprecated in Java", level = DeprecationLevel.HIDDEN)
    override fun loadContext(requestResponseHolder: org.springframework.security.web.context.HttpRequestResponseHolder): SecurityContext? {
        val context = loadContext(requestResponseHolder.request).get()
        return context ?: SecurityContextHolder.createEmptyContext()
    }

    override fun loadContext(request: HttpServletRequest): Supplier<SecurityContext?> {
        return Supplier { request.getAttribute(requestAttributeName) as SecurityContext? ?: SecurityContextHolder.createEmptyContext() }
    }

    override fun saveContext(context: SecurityContext?, request: HttpServletRequest, response: HttpServletResponse?) {
        request.setAttribute(requestAttributeName, context)
    }
}