package ru.magmel.infrastructure.digitalprofile.mq

import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.domain.DigitalProfileListener
import ru.magmel.domain.model.LoanApplication
import ru.magmel.domain.process.DigitalProfileProcessor
import ru.magmel.infrastructure.digitalprofile.mq.mapper.DigitalProfileMapper
import ru.magmel.infrastructure.digitalprofile.mq.model.DigitalProfileResponse
import ru.magmel.infrastructure.framework.mq.AbstractJmsListener
import ru.magmel.util.withException
import ru.magmel.util.withJmsMessageId
import java.util.UUID
import javax.jms.Message

@Component
class DigitalProfileJmsListener(
    private val loanApplicationService: LoanApplicationService,
    private val digitalProfileMapper: DigitalProfileMapper,
    private val digitalProfileProcessor: DigitalProfileProcessor
) : DigitalProfileListener, AbstractJmsListener() {

    private val log = LoggerFactory.getLogger(javaClass)

    @JmsListener(
        destination = "\${integration.digitalProfile.responseQueue}",
        containerFactory = "jmsListenerContainerFactory"
    )
    override fun onMessage(@Payload message: Message) {
        runCatching {
            val jmsMessageId = message.jmsMessageID
            val jmsCorrelationId = message.jmsCorrelationID
            log.info("Receive {} {}", withJmsMessageId(jmsMessageId), jmsCorrelationId)
            val response = getMessagePayload(message).also { messagePayload ->
                log.info("MessagePayload {} {} {}", messagePayload, withJmsMessageId(jmsMessageId), jmsCorrelationId)
            }
            processResponse(response).also {
                log.info("Response {} {} {}", response, withJmsMessageId(jmsMessageId), jmsCorrelationId)
            }
        }.onFailure { ex ->
            log.error("{}", withException(ex))
        }
    }

    override fun processResponse(response: String) {
        val digitalProfileResponse = extractResponse<DigitalProfileResponse>(response)
        loanApplicationService.getAndSave(UUID.fromString(digitalProfileResponse.id)) { loanApplication ->
            digitalProfileMapper.mapToDomain(digitalProfileResponse, loanApplication)
        }.let(::receive)
    }

    override fun receive(loanApplication: LoanApplication): String {
        return digitalProfileProcessor.process(loanApplication.id.toString())
    }
}