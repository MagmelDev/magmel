package ru.magmel.framework.properties

import ru.magmel.framework.config.JmsAbstractConfig

data class MqManagerProperties(
    var hostName: String? = null,
    var port: Int = 0,
    var channel: String? = null,
    var queueManager: String? = null,
    var timeout: Int = JmsAbstractConfig.DEFAULT_TIMEOUT,
    var username: String = "",
    var password: String = ""
)