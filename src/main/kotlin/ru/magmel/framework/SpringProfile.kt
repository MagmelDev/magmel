package ru.magmel.framework

 class SpringProfile {
     companion object {
         const val LOCAL = "local"
         const val TEST = "test"
         const val PREVIEW = "preview"
         const val PROD = "prod"
     }
 }