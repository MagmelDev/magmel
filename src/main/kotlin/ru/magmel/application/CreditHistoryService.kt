package ru.magmel.application

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.magmel.application.canon.ClientService
import ru.magmel.domain.CreditHistoryReceiver
import ru.magmel.domain.model.Client
import ru.magmel.util.withIdentifier

@Service
@Transactional
class CreditHistoryService(
    private val clientService: ClientService,
    private val creditHistoryReceiver: CreditHistoryReceiver
) {

    private val log = LoggerFactory.getLogger(javaClass)

    fun get(identifier: String): Client {
        log.info("Getting credit history {}", withIdentifier(identifier))
        return clientService.getAndSave(identifier) { client ->
            client.getCreditHistory(creditHistoryReceiver).also {
                log.info("Got credit history {}", withIdentifier(identifier))
            }
        }
    }
}