package ru.magmel.domain.model

import ru.magmel.domain.ClientIdentifierSearcher
import ru.magmel.domain.CreditHistoryReceiver
import java.util.*

open class Client(
    val id: UUID = UUID.randomUUID(),
    var name: Name,
    private var _clientBankData: ClientBankData? = null,
    private var _creditHistory: CreditHistory? = null
) {

    fun findIdentifier(clientIdentifierSearcher: ClientIdentifierSearcher) =
        this.apply {
            clientIdentifierSearcher.findByName(name)
                .onSuccess { identifier ->
                    this._clientBankData = ClientBankData(identifier = identifier)
                }.getOrThrow()
        }

    fun getCreditHistory(creditHistoryReceiver: CreditHistoryReceiver) =
        this.apply {
            clientBankData?.identifier?.let { identifier ->
                creditHistoryReceiver.get(identifier)
                    .onSuccess { status ->
                        this._creditHistory = CreditHistory(status = status)
                    }.getOrThrow()
            } ?: throw IllegalStateException("Identifier must be not null before credit history getting!")
        }

    val clientBankData: ClientBankData?
        get() {
            return if (_clientBankData == null) {
                null
            } else _clientBankData ?: throw AssertionError("Set to null by another thread")
        }

    val creditHistory: CreditHistory?
        get() {
            return if (_creditHistory == null) {
                null
            } else _creditHistory ?: throw AssertionError("Set to null by another thread")
        }
}