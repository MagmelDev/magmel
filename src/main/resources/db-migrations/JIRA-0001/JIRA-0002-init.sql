create table core.loan_application
(
    id uuid not null
        constraint pk_loan_application
            primary key,
    risk_decision varchar(255)
);

create table core.client_bank_data
(
    id uuid not null
        constraint pk_client_bank_data
            primary key,
    identifier varchar(255) unique
);

create table core.credit_history
(
    id uuid not null
        constraint pk_credit_history
            primary key,
    status varchar(255)
);

create table core.client
(
    id uuid not null
        constraint pk_client
            primary key,
    type varchar(255) not null,
    last_name varchar(255) not null,
    first_name varchar(255) not null,
    middle_name varchar(255),
    client_bank_data_id uuid
        constraint fk_client_client_bank_data_id
            references core.client_bank_data,
    credit_history_id uuid
        constraint fk_client_credit_history_id
            references core.credit_history
);

create table core.comment
(
    id uuid not null
        constraint pk_comment
            primary key,
    text varchar(255) not null
);

create table core.loan_application_clients
(
    loan_application_id uuid not null
        constraint fk_loan_application_clients_loan_application_id
            references core.loan_application,
    clients_id uuid not null
        constraint u_loan_application_clients_clients_id
            unique
        constraint fk_loan_application_clients_clients_id
            references core.client
);

create table core.loan_application_comments
(
    loan_application_id uuid not null
        constraint fk_loan_application_comments_loan_application_id
            references core.loan_application,
    comments_id uuid not null
        constraint u_loan_application_comments_comments_id
            unique
        constraint fk_loan_application_comments_comments_id
            references core.comment
);