package ru.magmel.domain

interface CreditHistoryReceiver {
    fun get(identifier: String): Result<String?>
}