package ru.magmel.domain.repository

import ru.magmel.domain.model.Client
import java.util.*

interface ClientRepository {
    fun findByLoanApplicationId(id: UUID): List<Client>
    fun save(client: Client): Client
    fun find(id: UUID): Client?
    fun find(identifier: String): Client?
    fun get(id: UUID): Client
    fun get(identifier: String): Client
}