package ru.magmel.infrastructure.partnerportal.mq

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.jms.core.JmsTemplate
import org.springframework.stereotype.Component
import ru.magmel.domain.PartnerPortalSender
import ru.magmel.domain.model.LoanApplication
import ru.magmel.infrastructure.framework.mq.AbstractJmsSender
import ru.magmel.infrastructure.partnerportal.mq.mapper.PartnerPortalMapper
import ru.magmel.infrastructure.partnerportal.mq.model.PartnerPortalRequest
import ru.magmel.util.withJmsMessageId
import java.util.concurrent.atomic.AtomicReference
import javax.jms.TextMessage

@Component
class PartnerPortalJmsSender(
    private val partnerPortalMapper: PartnerPortalMapper,
    private val jmsTemplate: JmsTemplate,
    @Value("\${integration.partnerPortal.requestQueue}") private val requestQueue: String,
    @Value("\${integration.partnerPortal.responseQueue}") private val responseQueue: String
) : PartnerPortalSender, AbstractJmsSender() {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun send(loanApplication: LoanApplication): String {
        return loanApplication.let(partnerPortalMapper::mapToRequest)
            .let(::send)
    }

    private fun send(request: PartnerPortalRequest): String {
        val requestMessage = parseSoapMessage<PartnerPortalRequest>(request).also { requestMessage ->
            log.info("RequestMessage {}", requestMessage)
        }
        return send(requestMessage).also { jmsMessageID ->
            log.info("Send {} {}", request.id, withJmsMessageId(jmsMessageID))
        }
    }

    private fun send(requestMessage: String): String {
        val messageReference = AtomicReference<TextMessage>()
        jmsTemplate.send(requestQueue) { session ->
            val textMessage = session.createTextMessage(requestMessage)
            textMessage.jmsReplyTo = session.createQueue(responseQueue)
            messageReference.set(textMessage)
            messageReference.get()
        }
        return messageReference.get().jmsMessageID
    }
}