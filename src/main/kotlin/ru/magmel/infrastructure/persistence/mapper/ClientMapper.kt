package ru.magmel.infrastructure.persistence.mapper

import ru.magmel.domain.model.Borrower as DomainBorrower
import ru.magmel.domain.model.Client as DomainClient
import ru.magmel.domain.model.ClientBankData as DomainClientBankData
import ru.magmel.domain.model.CreditHistory as DomainCreditHistory
import ru.magmel.domain.model.Name as DomainName
import ru.magmel.domain.model.Pledger as DomainPledger
import ru.magmel.infrastructure.persistence.model.Client
import ru.magmel.infrastructure.persistence.model.ClientBankData
import ru.magmel.infrastructure.persistence.model.ClientType
import ru.magmel.infrastructure.persistence.model.CreditHistory
import ru.magmel.infrastructure.persistence.model.Name

object ClientMapper {

    fun toPersistence(domainClient: DomainClient, client: Client): Client {
        return client.apply {
            name = toPersistence(domainClient.name)
            clientBankData = toPersistence(domainClient.clientBankData)
            creditHistory = toPersistence(domainClient.creditHistory)
        }
    }

    fun toPersistence(domainBorrower: DomainBorrower): Client {
        return Client(
            id = domainBorrower.id,
            type = ClientType.BORROWER,
            name = toPersistence(domainBorrower.name),
            clientBankData = toPersistence(domainBorrower.clientBankData),
            creditHistory = toPersistence(domainBorrower.creditHistory)
        )
    }

    fun toPersistence(domainBorrower: DomainBorrower, client: Client): Client {
        return client.apply {
            name = toPersistence(domainBorrower.name)
            clientBankData = toPersistence(domainBorrower.clientBankData)
            creditHistory = toPersistence(domainBorrower.creditHistory)
        }
    }

    fun toPersistence(domainPledger: DomainPledger): Client {
        return Client(
            id = domainPledger.id,
            type = ClientType.PLEDGER,
            name = toPersistence(domainPledger.name),
            clientBankData = toPersistence(domainPledger.clientBankData),
            creditHistory = toPersistence(domainPledger.creditHistory)
        )
    }

    fun toPersistence(domainPledger: DomainPledger, client: Client): Client {
        return client.apply {
            name = toPersistence(domainPledger.name)
            clientBankData = toPersistence(domainPledger.clientBankData)
            creditHistory = toPersistence(domainPledger.creditHistory)
        }
    }

    fun toDomain(client: Client): DomainClient {
        return DomainClient(
            id = client.id,
            name = toDomain(client.name),
            _clientBankData = toDomain(client.clientBankData),
            _creditHistory = toDomain(client.creditHistory)
        )
    }

    fun toDomainBorrower(client: Client): DomainBorrower {
        return DomainBorrower(
            id = client.id,
            name = toDomain(client.name),
            clientBankData = toDomain(client.clientBankData),
            creditHistory = toDomain(client.creditHistory)
        )
    }

    fun toDomainPledger(client: Client): DomainPledger {
        return DomainPledger(
            id = client.id,
            name = toDomain(client.name),
            clientBankData = toDomain(client.clientBankData),
            creditHistory = toDomain(client.creditHistory)
        )
    }

    private fun toPersistence(domainName: DomainName): Name {
        return Name(
            lastName = domainName.lastName,
            firstName = domainName.firstName,
            middleName = domainName.middleName
        )
    }

    private fun toDomain(name: Name): DomainName {
        return DomainName(
            lastName = name.lastName,
            firstName = name.firstName,
            middleName = name.middleName
        )
    }

    private fun toDomain(clientBankData: ClientBankData?): DomainClientBankData? {
        return clientBankData?.let {
            DomainClientBankData(
                id = clientBankData.id,
                identifier = clientBankData.identifier
            )
        }
    }

    private fun toPersistence(domainClientBankData: DomainClientBankData?): ClientBankData? {
        return domainClientBankData?.let {
            ClientBankData(
                id = domainClientBankData.id,
                identifier = domainClientBankData.identifier
            )
        }
    }

    private fun toDomain(creditHistory: CreditHistory?): DomainCreditHistory? {
        return creditHistory?.let {
            DomainCreditHistory(
                id = creditHistory.id,
                status = creditHistory.status
            )
        }
    }

    private fun toPersistence(domainCreditHistory: DomainCreditHistory?): CreditHistory? {
        return domainCreditHistory?.let {
            CreditHistory(
                id = domainCreditHistory.id,
                status = domainCreditHistory.status
            )
        }
    }
}