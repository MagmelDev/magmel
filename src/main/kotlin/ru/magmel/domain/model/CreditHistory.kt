package ru.magmel.domain.model

import org.springframework.util.Assert
import java.util.*

class CreditHistory(
    val id: UUID = UUID.randomUUID(),
    val status: String? = null
) {

    init {
        status?.let {
            Assert.state(it.isNotBlank(), EMPTY_STATUS)
        }
    }

    companion object {
        const val EMPTY_STATUS = "EMPTY_STATUS"
    }
}