package ru.magmel.interfaces.rest

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.magmel.application.DigitalProfileService
import ru.magmel.security.config.annotation.AuthorizeMagmel
import java.util.UUID

@RestController
@RequestMapping("api/magmel/digital-profile")
class DigitalProfileController(
    private val digitalProfileService: DigitalProfileService
) {

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PostMapping("/{id}")
    @Operation(summary = "Invoke integration")
    @ApiResponses(
        ApiResponse(responseCode = "202", description = "Accepted"),
        ApiResponse(responseCode = "500", description = "Unexpected error")
    )
    @AuthorizeMagmel
    fun invokeIntegration(id: String) {
        digitalProfileService.send(UUID.fromString(id))
    }
}