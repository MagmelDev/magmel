package ru.magmel.interfaces.dto.get

data class GetNameDto(
    val lastName: String,
    val firstName: String,
    val middleName: String?
)
