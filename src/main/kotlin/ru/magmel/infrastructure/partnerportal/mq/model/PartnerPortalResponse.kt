package ru.magmel.infrastructure.partnerportal.mq.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@XmlRootElement(name = "PartnerPortalResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartnerPortalResponse", propOrder = ["id", "status", "comment"])
data class PartnerPortalResponse(

    @field:XmlElement(name = "id", required = true)
    var id: String? = null,

    @field:XmlElement(name = "status", required = true)
    var status: String? = null,

    @field:XmlElement(name = "comment")
    var comment: String? = null
)