package ru.magmel.application.canon

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.magmel.domain.model.LoanApplication
import ru.magmel.domain.repository.LoanApplicationRepository
import ru.magmel.util.withBusinessKey
import java.util.UUID

@Service
class LoanApplicationService(
    private val loanApplicationRepository: LoanApplicationRepository
) {

    private val log = LoggerFactory.getLogger(javaClass)

    @Transactional
    fun create(loanApplication: LoanApplication) =
        loanApplicationRepository.save(loanApplication).also {
            log.info("Created {}", withBusinessKey(loanApplication.id))
        }

    @Transactional(readOnly = true)
    fun find(id: UUID) = loanApplicationRepository.find(id) ?: null.also {
        log.warn(
            "LoanApplication not found {}",
            withBusinessKey(id)
        )
    }

    @Transactional(readOnly = true)
    fun getReadOnly(id: UUID) = loanApplicationRepository.get(id).also {
        log.info("Get {}", withBusinessKey(it.id))
    }

    @Transactional
    fun getAndSave(id: UUID, buildAction: (LoanApplication) -> LoanApplication) =
        loanApplicationRepository.get(id).also {
            log.info("Get {}", withBusinessKey(it.id))
        }.apply {
            buildAction(this)
        }.let(loanApplicationRepository::save).also {
            log.info("Updated {}", withBusinessKey(it.id))
        }
}