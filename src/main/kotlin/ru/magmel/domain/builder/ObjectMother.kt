package ru.magmel.domain.builder

object ObjectMother {
    fun loanApplication() = LoanApplicationBuilder()
    fun borrower() = BorrowerBuilder()
    fun pledger() = PledgerBuilder()
}