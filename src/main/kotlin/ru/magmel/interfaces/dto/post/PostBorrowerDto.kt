package ru.magmel.interfaces.dto.post

import javax.validation.Valid

class PostBorrowerDto(
    @field:Valid
    val name: PostNameDto
)