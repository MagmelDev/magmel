package ru.magmel.infrastructure.clientsearcher

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import ru.magmel.domain.ClientIdentifierSearcher
import ru.magmel.domain.model.Name
import ru.magmel.util.withException
import ru.magmel.infrastructure.clientsearcher.model.Response

@Component
class HttpClientIdentifierSearcher(
    private val restTemplate: RestTemplate,
    @Value("\${integration.clientsearcher.host}") private val host: String
) : ClientIdentifierSearcher {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun findByName(name: Name): Result<String?> {
        return runCatching {
            val middleNameUrlPart = name.middleName?.let { "&middleName=$it" }.orEmpty()
            val response = restTemplate.getForEntity(
                "$host/search?lastName=${name.lastName}&firstName=${name.firstName}$middleNameUrlPart",
                Response::class.java
            )
            response.body!!.client.identifier
        }.onSuccess { identifier ->
            log.info("Success! Found identifier=$identifier")
        }.onFailure { ex ->
            log.error("Unexpected error {}", withException(ex))
        }
    }
}