package ru.magmel.infrastructure.persistence.repository

import org.springframework.stereotype.Repository
import ru.magmel.domain.model.LoanApplication
import ru.magmel.domain.repository.LoanApplicationRepository
import ru.magmel.infrastructure.persistence.mapper.LoanApplicationMapper
import ru.magmel.infrastructure.persistence.repository.imported.ImportedLoanApplicationJpaRepository
import java.util.*

@Repository
class LoanApplicationJpaRepository(
    private val impl: ImportedLoanApplicationJpaRepository
) : LoanApplicationRepository {
    override fun save(loanApplication: LoanApplication): LoanApplication =
        (LoanApplicationMapper::toPersistence)(loanApplication)
            .let(impl::save)
            .let(LoanApplicationMapper::toDomain)

    override fun find(id: UUID): LoanApplication? =
        impl.findById(id).takeIf { it.isPresent }?.get()?.let(LoanApplicationMapper::toDomain)

    override fun get(id: UUID): LoanApplication =
        impl.getReferenceById(id).let(LoanApplicationMapper::toDomain)
}
