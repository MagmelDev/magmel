package ru.magmel.infrastructure.camunda.worker

import org.camunda.bpm.engine.ExternalTaskService
import org.camunda.bpm.engine.externaltask.LockedExternalTask
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.magmel.application.canon.ClientService
import ru.magmel.domain.builder.VariablesBuilder
import ru.magmel.domain.model.Client
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withException
import ru.magmel.infrastructure.camunda.framework.businessKey
import ru.magmel.infrastructure.camunda.framework.NoncriticalWorker

/**
 * main.bpmn - Client Identifier Searcher
 */
@Component
class ClientIdentifierSearcherWorker(
    override val externalTaskService: ExternalTaskService,
    @Value("\${process.worker.clientIdentifierSearcherWorker.initRetries}") override val initRetries: Int,
    @Value("\${process.worker.clientIdentifierSearcherWorker.retryTimeout}") override val retryTimeout: Long,
    @Value("\${process.worker.clientIdentifierSearcherWorker.lockDuration}") override val lockDuration: Long,
    @Value("\${process.worker.clientIdentifierSearcherWorker.maxTasks}") override val maxTasks: Int,
    private val clientService: ClientService
) : NoncriticalWorker(topicName = "ClientIdentifierSearcherTopicName", workerId = "ClientIdentifierSearcherWorkerId") {

    override val log: Logger = LoggerFactory.getLogger(javaClass)

    @Scheduled(fixedDelay = POLLING_TIME)
    override fun scheduled() {
        super.fetchExternalTasks()
    }

    override fun execute(lockedExternalTask: LockedExternalTask) {
        runCatching {
            clientService.searchClientIdentifiersByLoanApplicationId(lockedExternalTask.businessKey())
        }.onSuccess { clients ->
            val variables = buildVariables(clients)
            complete(lockedExternalTask, variables)
        }.onFailure { ex ->
            val errorMessage = "errorMessage"
            log.warn(
                "Unexpected error occurred {} {}",
                withBusinessKey(lockedExternalTask.businessKey),
                withException(ex)
            )
            handleNoncriticalFailure(
                lockedExternalTask,
                errorMessage = errorMessage,
                errorDetails = "errorDetails",
                variables = buildVariables(emptyList())
            )
        }
    }

    private fun buildVariables(clients: List<Client>) =
        VariablesBuilder()
            .identifiers(clients)
            .please()

    companion object {
        private const val POLLING_TIME: Long = 1000
    }
}