package ru.magmel.domain

import ru.magmel.domain.model.LoanApplication

interface DigitalProfileListener {
    fun receive(loanApplication: LoanApplication): String
}