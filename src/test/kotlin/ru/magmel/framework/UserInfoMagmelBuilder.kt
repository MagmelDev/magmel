package ru.magmel.framework

import com.nimbusds.jose.util.Base64
import ru.magmel.security.authentication.MagmelRole
import ru.magmel.security.authentication.UserInfoMagmel

class UserInfoMagmelBuilder(private val username: String = "magmel") {

    private val roles = mutableSetOf<MagmelRole>()

    private var aud = "magmel-api"

    fun pmm() = apply {
        roles.add(MagmelRole.PersonalMortgageManager)
    }

    fun ca() = apply {
        roles.add(MagmelRole.CreditAdministrator)
    }

    fun dm() = apply {
        roles.add(MagmelRole.DealManager)
    }

    fun withInvalidAud() = apply {
        aud = "invalid-api"
    }

    private fun mapRoles() = roles.takeIf { it.isNotEmpty() }?.joinToString { "\"${it.value}\"" }
        ?: throw Exception("Roles must not be empty!")

    fun please(): UserInfoMagmel {
        val userInfo = UserInfoMagmel(userName = username, clientId = null.toString(), audience = null.toString())
        userInfo.roles = roles.toList()
        return userInfo
    }

    fun asStringPlease(): String {
        val token = """
            {
              "exp": 1652964905,
              "iat": 1652964785,
              "auth_time": 1652964777,
              "jti": "461ac52a-c861-41b5-b131-2a37a239c733",
              "iss": "https://keycloak.magmel.ru/realms/magmel",
              "aud": [
                "$aud",
                "account"
              ],
              "sub": "4a6504af-46f6-4a93-a7d8-4587a2ae530a",
              "typ": "Bearer",
              "azp": "magmel-front",
              "session_state": "d6639c7c-2951-4413-b2da-2be171a94b86",
              "acr": "1",
              "allowed-origins": [
                "https://keycloak.magmel.ru"
              ],
              "realm_access": {
                "roles": [
                  "default-roles-magmel",
                  "offline_access",
                  "uma_authorization"
                ]
              },
              "resource_access": {
                "magmel-api": {
                  "roles": [
                    ${mapRoles()}
                  ]
                },
                "account": {
                  "roles": [
                    "manage-account",
                    "manage-account-links",
                    "view-profile"
                  ]
                },
                "magmel-front": {
                  "roles": [
                     ${mapRoles()}
                  ]
                }
              },
              "scope": "profile email",
              "sid": "d6639c7c-2951-4413-b2da-2be171a94b86",
              "email_verified": false,
              "name": "KUZMIN Dmitry KUZMIN",
              "preferred_username": $username,
              "given_name": "KUZMIN Dmitry",
              "family_name": "KUZMIN",
              "email": "dmitry.k.kuzmin@magmel.ru",
              "client_id": "magmel-front"
            }
        """.trimIndent()
        return Base64.encode(token).toString()
    }
}
