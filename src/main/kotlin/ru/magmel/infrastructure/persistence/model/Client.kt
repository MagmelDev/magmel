package ru.magmel.infrastructure.persistence.model

import ru.magmel.infrastructure.persistence.Schema
import java.util.*
import javax.persistence.CascadeType
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "CLIENT", schema = Schema.CORE)
data class Client(
    @Id
    val id: UUID,

    @Enumerated(EnumType.STRING)
    var type: ClientType,

    @Embedded
    var name: Name,

    @OneToOne(cascade = [CascadeType.ALL])
    var clientBankData: ClientBankData? = null,

    @OneToOne(cascade = [CascadeType.ALL])
    var creditHistory: CreditHistory? = null
)