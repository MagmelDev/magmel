package ru.magmel.infrastructure.riskcalculator.model

data class Request(
    val identifiers: List<String>
)