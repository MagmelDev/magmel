package ru.magmel.application

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.domain.RiskCalculator
import ru.magmel.domain.model.RiskDecision
import ru.magmel.util.withBusinessKey
import java.util.UUID

@Service
@Transactional
class RiskService(
    private val loanApplicationService: LoanApplicationService,
    private val riskCalculator: RiskCalculator
) {

    private val log = LoggerFactory.getLogger(javaClass)

    fun calculateRisk(id: UUID): RiskDecision {
        log.info("Calculating risks {}", withBusinessKey(id))
        return loanApplicationService.getAndSave(id) { loanApplication ->
            loanApplication.calculateRisk(riskCalculator).also {
                log.info("Risk calculated id=${it.id} riskDecision=${it.riskDecision}")
            }
        }.riskDecision ?: throw IllegalStateException("Risk decision must be not null after calculating!")
    }
}