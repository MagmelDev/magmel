package ru.magmel.domain.builder

import ru.magmel.domain.model.Name

object NameHelper {

    fun vladimirMayakovsky() = Name(
        lastName = "Маяковский",
        firstName = "Владимир",
        middleName = "Владимирович"
    )

    fun mikhailBulgakov() = Name(
        lastName = "Булгаков",
        firstName = "Михаил",
        middleName = "Афанасьевич"
    )

    fun andrzejSapkowski() = Name(
        lastName = "Сапковский",
        firstName = "Анджей",
        middleName = null
    )

    fun charlesMaclean() = Name(
        lastName = "Маклин",
        firstName = "Чарльз",
        middleName = null
    )
}