package ru.magmel.infrastructure.camunda.framework

import org.camunda.bpm.engine.ExternalTaskService
import org.camunda.bpm.engine.externaltask.LockedExternalTask
import org.slf4j.Logger
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withElapsedTime
import ru.magmel.util.withErrorCode
import ru.magmel.util.withVariables
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.system.measureTimeMillis

abstract class Worker(
    val workerId: String,
    private val topicName: String
) {
    abstract val externalTaskService: ExternalTaskService
    abstract val lockDuration: Long
    abstract val maxTasks: Int
    abstract val log: Logger

    private val lock = ReentrantLock()

    fun fetchExternalTasks() {
        lock.withLock {
            externalTaskService
                .fetchAndLock(maxTasks, workerId)
                .topic(topicName, lockDuration)
                .execute()
                .forEach(::execute)
        }
    }

    abstract fun scheduled()

    abstract fun execute(lockedExternalTask: LockedExternalTask)

    fun complete(lockedExternalTask: LockedExternalTask, variables: Map<String, Any?> = mapOf()) {
        measureTimeMillis {
            externalTaskService.complete(lockedExternalTask.id, workerId, variables)
        }.let { elapsedTime ->
            log.info(
                "Complete locked external task {} {} in {}",
                withBusinessKey(lockedExternalTask.businessKey),
                withVariables(variables),
                withElapsedTime(elapsedTime)
            )
        }
    }

    fun handleBpmError(lockedExternalTask: LockedExternalTask, errorCode: String) {
        measureTimeMillis {
            externalTaskService.handleBpmnError(lockedExternalTask.id, workerId, errorCode)
        }.let { elapsedTime ->
            log.info(
                "Handle bpm error {} {} in {}",
                withBusinessKey(lockedExternalTask.businessKey),
                withErrorCode(errorCode),
                withElapsedTime(elapsedTime)
            )
        }
    }

    companion object {
        private const val LOCK_DURATION_BELOW_ZERO = "LOCK_DURATION_BELOW_ZERO"
    }
}