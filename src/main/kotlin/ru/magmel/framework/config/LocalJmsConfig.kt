package ru.magmel.framework.config

import org.apache.activemq.ActiveMQConnectionFactory
import org.apache.activemq.broker.BrokerService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jms.annotation.EnableJms
import org.springframework.jms.config.DefaultJmsListenerContainerFactory
import org.springframework.jms.config.JmsListenerContainerFactory
import org.springframework.jms.core.JmsTemplate
import ru.magmel.framework.SpringProfile
import ru.magmel.framework.properties.MqManagerProperties
import javax.jms.ConnectionFactory
import javax.jms.JMSException

@Configuration
@EnableJms
@Profile(value = [SpringProfile.LOCAL])
class LocalJmsConfig : JmsAbstractConfig {

    @Bean
    @Throws(JMSException::class)
    fun jmsConnectionFactory(): ConnectionFactory =
        ActiveMQConnectionFactory(BROKER_URL)

    @Bean
    fun jmsTemplate(
        jmsConnectionFactory: ConnectionFactory,
        mqManagerProperties: MqManagerProperties
    ): JmsTemplate =
        createJmsTemplate(jmsConnectionFactory, mqManagerProperties.timeout)

    @Bean
    fun jmsListenerContainerFactory(
        jmsConnectionFactory: ConnectionFactory
    ): JmsListenerContainerFactory<*> =
        DefaultJmsListenerContainerFactory().apply {
            setConnectionFactory(jmsConnectionFactory)
        }

    @Bean
    fun broker(): BrokerService =
        BrokerService().apply {
            addConnector(BROKER_URL)
            isPersistent = false
        }

    companion object {
        private const val BROKER_URL = "tcp://localhost:61615"
    }
}