package ru.magmel.framework.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.magmel.framework.properties.MqManagerProperties

@Configuration
class MqPropertiesConfig {

    @Bean
    @ConfigurationProperties(prefix = "integration.mq")
    fun mqProperties() = MqManagerProperties()
}