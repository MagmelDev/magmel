package ru.magmel.security.config.annotation

import org.springframework.security.access.prepost.PreAuthorize
import ru.magmel.security.Constants
import ru.magmel.security.authentication.MagmelRole

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@Retention(AnnotationRetention.RUNTIME)
@PreAuthorize("hasAuthority('${Constants.AUTHORITIES_MAGMEL}')")
annotation class AuthorizeMagmel

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@Retention(AnnotationRetention.RUNTIME)
@PreAuthorize("hasAuthority('${Constants.AUTHORITIES_MAGMEL}') and hasAnyRole('${MagmelRole.PERSONAL_MORTGAGE_MANAGER}')")
annotation class AuthorizeMagmelPersonalMortgageManager

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@Retention(AnnotationRetention.RUNTIME)
@PreAuthorize("hasAuthority('${Constants.AUTHORITIES_MAGMEL}') and hasAnyRole('${MagmelRole.CREDIT_ADMINISTRATOR}')")
annotation class AuthorizeMagmelCreditAdministrator

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@Retention(AnnotationRetention.RUNTIME)
@PreAuthorize("hasAuthority('${Constants.AUTHORITIES_MAGMEL}') and hasAnyRole('${MagmelRole.DEAL_MANAGER}')")
annotation class AuthorizeMagmelDealManager
