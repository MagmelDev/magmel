package ru.magmel.interfaces.assembler

import ru.magmel.domain.model.Name
import ru.magmel.interfaces.dto.get.GetNameDto
import ru.magmel.interfaces.dto.post.PostNameDto

object NameAssembler {
    fun toDomain(name: PostNameDto): Name =
        Name(
            lastName = name.lastName,
            firstName = name.firstName,
            middleName = name.middleName
        )

    fun toDto(name: Name): GetNameDto =
        GetNameDto(
            lastName = name.lastName,
            firstName = name.firstName,
            middleName = name.middleName
        )
}