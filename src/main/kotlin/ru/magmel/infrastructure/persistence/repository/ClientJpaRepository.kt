package ru.magmel.infrastructure.persistence.repository

import ru.magmel.domain.model.Client as DomainClient
import org.springframework.stereotype.Repository
import ru.magmel.domain.repository.ClientRepository
import ru.magmel.infrastructure.persistence.mapper.ClientMapper
import ru.magmel.infrastructure.persistence.repository.imported.ImportedClientJpaRepository
import java.util.*

@Repository
class ClientJpaRepository(
    private val impl: ImportedClientJpaRepository
) : ClientRepository {
    override fun findByLoanApplicationId(id: UUID): List<DomainClient> =
        impl.findAllByLoanApplicationId(id).map(ClientMapper::toDomain)

    override fun save(client: DomainClient): DomainClient {
        return impl.getReferenceById(client.id).let {
            ClientMapper.toPersistence(client, it)
        }.let(impl::save).let(ClientMapper::toDomain)
    }

    override fun find(id: UUID): DomainClient? =
        impl.findById(id).takeIf { it.isPresent }?.get()?.let(ClientMapper::toDomain)

    override fun find(identifier: String): DomainClient? =
        impl.findByClientBankDataIdentifier(identifier)?.let(ClientMapper::toDomain)

    override fun get(id: UUID): DomainClient =
        impl.getReferenceById(id).let(ClientMapper::toDomain)

    override fun get(identifier: String): DomainClient =
        impl.getByClientBankDataIdentifier(identifier).let(ClientMapper::toDomain)
}
