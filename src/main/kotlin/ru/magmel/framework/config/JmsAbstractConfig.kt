package ru.magmel.framework.config

import com.ibm.mq.jms.MQQueueConnectionFactory
import com.ibm.msg.client.wmq.WMQConstants
import org.springframework.jms.connection.UserCredentialsConnectionFactoryAdapter
import org.springframework.jms.core.JmsTemplate
import ru.magmel.framework.properties.MqManagerProperties
import javax.jms.ConnectionFactory
import javax.jms.DeliveryMode
import javax.jms.JMSException

interface JmsAbstractConfig {

    @Throws(JMSException::class)
    fun createConnectionFactory(mq: MqManagerProperties): ConnectionFactory =
        UserCredentialsConnectionFactoryAdapter().apply {
            setTargetConnectionFactory(
                MQQueueConnectionFactory().apply {
                    hostName = mq.hostName
                    channel = mq.channel
                    port = mq.port
                    queueManager = mq.queueManager
                    transportType = WMQConstants.WMQ_CM_CLIENT
                }
            )
            setUsername(mq.username)
            setPassword(mq.password)
        }

    fun createJmsTemplate(jmsConnectionFactory: ConnectionFactory, timeout: Int): JmsTemplate =
        JmsTemplate(jmsConnectionFactory).apply {
            isSessionTransacted = true
            deliveryMode = DeliveryMode.NON_PERSISTENT
            isMessageIdEnabled = false
            isMessageTimestampEnabled = false
            isPubSubNoLocal = true
            isExplicitQosEnabled = true
            receiveTimeout = timeout.toLong()

        }

    companion object {
        const val DEFAULT_TIMEOUT = 60_000
    }
}