package ru.magmel.framework.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.convert.Jsr310Converters
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EntityScan(basePackages = ["ru.magmel.infrastructure"], basePackageClasses = [Jsr310Converters::class])
@EnableJpaRepositories(basePackages = ["ru.magmel.infrastructure.persistence.repository"])
@EnableTransactionManagement
class JpaRepositoriesConfig