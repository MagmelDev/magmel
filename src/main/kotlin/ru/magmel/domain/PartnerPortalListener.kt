package ru.magmel.domain

import ru.magmel.domain.model.LoanApplication

interface PartnerPortalListener {
    fun receive(loanApplication: LoanApplication)
}