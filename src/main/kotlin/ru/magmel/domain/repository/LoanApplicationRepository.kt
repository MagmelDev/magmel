package ru.magmel.domain.repository

import ru.magmel.domain.model.LoanApplication
import java.util.*

interface LoanApplicationRepository {
    fun save(loanApplication: LoanApplication): LoanApplication
    fun find(id: UUID): LoanApplication?
    fun get(id: UUID): LoanApplication
}