package ru.magmel.infrastructure.camunda

import org.camunda.bpm.engine.RuntimeService
import org.springframework.stereotype.Component
import ru.magmel.domain.process.DigitalProfileProcessor

@Component
class CamundaDigitalProfileProcessor(
    private val runtimeService: RuntimeService
) : DigitalProfileProcessor {

    override fun process(businessKey: String, variables: Map<String, Any>): String {
        return runtimeService
            .createMessageCorrelation(PROCESS_DIGITAL_PROFILE)
            .processInstanceBusinessKey(businessKey)
            .setVariables(variables)
            .correlate().let { businessKey }
    }

    companion object {
        const val PROCESS_DIGITAL_PROFILE = "Message_ProcessDigitalProfile"
    }
}