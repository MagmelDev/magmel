package ru.magmel.infrastructure.framework.mq

import org.slf4j.LoggerFactory
import java.nio.charset.Charset
import javax.jms.BytesMessage
import javax.jms.JMSException
import javax.jms.Message
import javax.jms.TextMessage
import javax.xml.bind.JAXBContext
import javax.xml.soap.MessageFactory
import javax.xml.soap.MimeHeaders
import javax.xml.soap.SOAPBody

abstract class AbstractJmsListener {

    abstract fun onMessage(message: Message)

    abstract fun processResponse(response: String)

    private val log = LoggerFactory.getLogger(javaClass)

    inline fun <reified T> extractResponse(messagePayload: String): T {
        val soapContent = extractSoapContent(messagePayload)
        val document = if (soapContent != null && !soapContent.hasFault()) {
            soapContent.extractContentAsDocument()
        } else throw IllegalArgumentException("SoapContent must be not empty or has fault")
        return JAXBContext.newInstance(T::class.java).createUnmarshaller()
            .unmarshal(document) as T
    }

    fun getMessagePayload(message: Message, charset: Charset = Charset.forName("UTF-8")): String =
        when (message) {
            is TextMessage -> message.text
            is BytesMessage -> {
                val length = message.bodyLength.toInt()
                val textBytes = ByteArray(length)
                message.readBytes(textBytes, length)
                String(bytes = textBytes, charset = charset)
            }
            else -> throw JMSException("Unsupported message type")
        }

    fun extractSoapContent(messageContent: String?): SOAPBody? {
        if (messageContent == null || messageContent.isBlank())
            throw IllegalArgumentException("Soap message must be not null or empty")
        return MessageFactory
            .newInstance()
            .createMessage(MimeHeaders(), messageContent.byteInputStream())
            .soapBody.also { soapBody ->
                log.info("SoapBody {}", soapBody)
            }
    }
}