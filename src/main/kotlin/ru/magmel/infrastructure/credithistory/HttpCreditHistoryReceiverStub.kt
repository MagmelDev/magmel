package ru.magmel.infrastructure.credithistory

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.magmel.framework.SpringProfile
import java.nio.charset.Charset
import java.util.UUID

@RestController
@RequestMapping("stub/credit-history")
@Profile(value = [SpringProfile.LOCAL])
class HttpCreditHistoryReceiverStub {

    private val log = LoggerFactory.getLogger(javaClass)

    init {
        log.warn("${this.javaClass.name} stub is active!")
    }

    @GetMapping("client/{identifier}")
    fun search(
        @PathVariable identifier: String
    ): ResponseEntity<String> {
        val body = resolveResponse(identifier)
        return ResponseEntity.ok(body)
    }

    private fun resolveResponse(identifier: String): String {
        val uuid = UUID.randomUUID()
        log.info("Search fire uuid=$uuid, identifier=$identifier")
        return resource("/stub/credithistory/default-response.json")
    }

    private fun resource(fileName: String) = this.javaClass.getResource(fileName).readText(Charset.forName("UTF-8"))

}