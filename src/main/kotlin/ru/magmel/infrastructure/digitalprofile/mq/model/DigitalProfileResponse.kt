package ru.magmel.infrastructure.digitalprofile.mq.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@XmlRootElement(name = "DigitalProfileResponse")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DigitalProfileResponse", propOrder = ["id", "identifierChecks"])
data class DigitalProfileResponse(

    @field:XmlElement(name = "id", required = true)
    var id: String? = null,

    @field:XmlElement(name = "identifierChecks", required = true)
    var identifierChecks: IdentifierChecks? = null
)