package ru.magmel.infrastructure.persistence.repository.imported

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import ru.magmel.infrastructure.persistence.model.Client
import java.util.*

@Repository
interface ImportedClientJpaRepository : JpaRepository<Client, UUID> {

    @Query(value = "SELECT la.clients FROM LoanApplication la WHERE la.id = ?1")
    fun findAllByLoanApplicationId(id: UUID): List<Client>

    fun getByClientBankDataIdentifier(identifier: String): Client

    fun findByClientBankDataIdentifier(identifier: String): Client?
}
