package ru.magmel.infrastructure.camunda

import org.camunda.bpm.engine.RuntimeService
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.magmel.domain.process.PartnerPortalProcessor
import ru.magmel.util.withBusinessKey
import java.util.concurrent.ConcurrentHashMap

@Component
class CamundaPartnerPortalProcessor(
    private val runtimeService: RuntimeService
) : PartnerPortalProcessor {

    private val log = LoggerFactory.getLogger(javaClass)

    private val processingList = ConcurrentHashMap<String, Map<String, Any>>()

    override fun process(businessKey: String, variables: Map<String, Any>) {
        log.info("Add {} to process", withBusinessKey(businessKey))
        processingList[businessKey] = variables
    }

    @Scheduled(fixedDelay = 1000)
    private fun process() {
        processingList.iterator().let { i ->
            while (i.hasNext()) {
                i.next().let { (businessKey, variables) ->
                    log.info("Processing {}", withBusinessKey(businessKey))
                    runtimeService
                        .createMessageCorrelation(PROCESS_PARTNER_PORTAL)
                        .processInstanceBusinessKey(businessKey)
                        .setVariables(variables)
                        .correlate().let {
                            i.remove()
                        }
                }
            }
        }
    }

    companion object {
        const val PROCESS_PARTNER_PORTAL = "Message_ProcessPartnerPortal"
    }
}