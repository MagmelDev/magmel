package ru.magmel.application

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import ru.magmel.Application
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.domain.builder.ObjectMother
import spock.lang.Specification

@ContextConfiguration
@SpringBootTest(classes = [Application.class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LoanApplicationServiceMust extends Specification {

    @Autowired
    LoanApplicationService loanApplicationService

    def create = ObjectMother.INSTANCE

    def "create and find loan application"() {
        given:
        def newLoanApplication = create.loanApplication()
                .please()
                .addPledger(create.pledger().please())

        when:
        def createdLoanApplication = loanApplicationService.create(newLoanApplication)

        then:
        createdLoanApplication.id == newLoanApplication.id

        when:
        def uploadedLoanApplication = loanApplicationService.find(createdLoanApplication.id)

        then:
        uploadedLoanApplication.id == createdLoanApplication.id
        uploadedLoanApplication.borrowers[0].name.fullName == "Маяковский Владимир Владимирович"
        uploadedLoanApplication.pledgers[0].name.fullName == "Булгаков Михаил Афанасьевич"
    }
}
