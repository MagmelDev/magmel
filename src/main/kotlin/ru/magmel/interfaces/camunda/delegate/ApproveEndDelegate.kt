package ru.magmel.interfaces.camunda.delegate

import com.google.gson.GsonBuilder
import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.framework.aop.DelegateAudition
import ru.magmel.util.withJson
import java.util.UUID

@Component("ApproveEndDelegate")
class ApproveEndDelegate(
    private val loanApplicationService: LoanApplicationService
) : JavaDelegate {

    private val log = LoggerFactory.getLogger(javaClass)

    @DelegateAudition
    override fun execute(execution: DelegateExecution) {
        val businessKey = execution.businessKey
        val loanApplication = loanApplicationService.getReadOnly(UUID.fromString(businessKey))
        val json = GsonBuilder().setPrettyPrinting().create().toJson(loanApplication)
        log.info("LoanApplication {}", withJson(json))
    }
}