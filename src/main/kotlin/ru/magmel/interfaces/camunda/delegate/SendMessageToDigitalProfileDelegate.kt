package ru.magmel.interfaces.camunda.delegate

import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.springframework.stereotype.Component
import ru.magmel.application.DigitalProfileService
import ru.magmel.framework.aop.DelegateAudition
import java.util.UUID

@Component("SendMessageToDigitalProfileDelegate")
class SendMessageToDigitalProfileDelegate(
    private val digitalProfileService: DigitalProfileService
) : JavaDelegate {

    @DelegateAudition
    override fun execute(execution: DelegateExecution) {
        val businessKey = execution.businessKey
        digitalProfileService.send(UUID.fromString(businessKey))
    }
}