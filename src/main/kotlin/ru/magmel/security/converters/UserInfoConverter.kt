package ru.magmel.security.converters

import org.json.JSONObject
import org.springframework.security.core.Authentication

interface UserInfoConverter<T : Authentication> {
    fun canApplied(userInfo: JSONObject): Boolean
    fun decodeJson(userInfo: JSONObject): T
}