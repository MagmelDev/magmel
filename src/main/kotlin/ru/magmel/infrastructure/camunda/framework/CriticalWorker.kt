package ru.magmel.infrastructure.camunda.framework

import org.camunda.bpm.engine.externaltask.LockedExternalTask
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withElapsedTime
import ru.magmel.util.withErrorDetails
import ru.magmel.util.withErrorMessage
import kotlin.system.measureTimeMillis

abstract class CriticalWorker(
    workerId: String,
    topicName: String,
) : Worker(workerId, topicName) {

    abstract val initRetries: Int

    abstract val retryTimeout: Long

    fun handleCriticalFailure(
        lockedExternalTask: LockedExternalTask,
        errorCode: String,
        errorMessage: String,
        errorDetails: String
    ) {
        val leftRetries = (lockedExternalTask.retries ?: initRetries) - 1
        if (leftRetries < 1) {
            log.info(
                "Handle critical failure {} with {}. Retries left {}",
                withBusinessKey(lockedExternalTask.businessKey),
                withErrorMessage(errorMessage),
                leftRetries
            )
            handleBpmError(lockedExternalTask, errorCode)
        } else {
            measureTimeMillis {
                externalTaskService.handleFailure(
                    lockedExternalTask.id,
                    workerId,
                    errorMessage,
                    errorDetails,
                    leftRetries,
                    retryTimeout
                )
            }.let { duration ->
                log.info(
                    "Handle critical failure {} with {} {} in {}. Retries left {}",
                    withBusinessKey(lockedExternalTask.businessKey),
                    withErrorMessage(errorMessage),
                    withErrorDetails(errorDetails),
                    withElapsedTime(duration),
                    leftRetries
                )
            }
        }
    }
}