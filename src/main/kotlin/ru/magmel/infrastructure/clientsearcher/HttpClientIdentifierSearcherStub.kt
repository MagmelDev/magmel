package ru.magmel.infrastructure.clientsearcher

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.magmel.domain.builder.NameHelper
import ru.magmel.framework.SpringProfile
import java.nio.charset.Charset
import java.util.UUID

@RestController
@RequestMapping("stub/client-searcher")
@Profile(value = [SpringProfile.LOCAL])
class HttpClientIdentifierSearcherStub {

    private val log = LoggerFactory.getLogger(javaClass)

    init {
        log.warn("${this.javaClass.name} stub is active!")
    }

    @GetMapping("search")
    fun search(
        @RequestParam lastName: String,
        @RequestParam firstName: String,
        @RequestParam middleName: String?
    ): ResponseEntity<String> {
        val body = resolveResponse(lastName, firstName, middleName)
        return ResponseEntity.ok(body)
    }

    private fun resolveResponse(lastName: String, firstName: String, middleName: String?): String {
        val uuid = UUID.randomUUID()
        log.info("Search fire uuid=$uuid, lastName=$lastName, firstName=$firstName, middleName=$middleName")
        return when {
            (lastName == vladimirMayakovsky.lastName &&
                    firstName == vladimirMayakovsky.firstName &&
                    middleName == vladimirMayakovsky.middleName) ->
                resource("/stub/clientsearcher/default-borrower-response.json").also {
                    log.info("Resolve as Vladimir Mayakovsky uuid=$uuid")
                }
            (lastName == mikhailBulgakov.lastName &&
                    firstName == mikhailBulgakov.firstName &&
                    middleName == mikhailBulgakov.middleName) ->
                resource("/stub/clientsearcher/default-pledger-response.json").also {
                    log.info("Resolve as Mikhail Bulgakov uuid=$uuid")
                }
            (lastName == charlesMaclean.lastName &&
                    firstName == charlesMaclean.firstName &&
                    middleName == charlesMaclean.middleName) ->
                resource("/stub/clientsearcher/error-response.json").also {
                    log.error("Resolve as Charles Maclean uuid=$uuid")
                }
            else -> resource("/stub/clientsearcher/default-response.json").also {
                log.warn("Else case uuid=$uuid")
            }
        }
    }

    private fun resource(fileName: String) = this.javaClass.getResource(fileName).readText(Charset.forName("UTF-8"))

    private companion object {
        val vladimirMayakovsky = NameHelper.vladimirMayakovsky()
        val mikhailBulgakov = NameHelper.mikhailBulgakov()
        val charlesMaclean = NameHelper.charlesMaclean()
    }
}