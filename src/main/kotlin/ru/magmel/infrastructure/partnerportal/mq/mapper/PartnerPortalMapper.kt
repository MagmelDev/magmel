package ru.magmel.infrastructure.partnerportal.mq.mapper

import org.springframework.stereotype.Component
import ru.magmel.domain.model.Comment
import ru.magmel.domain.model.LoanApplication
import ru.magmel.infrastructure.partnerportal.mq.model.PartnerPortalRequest
import ru.magmel.infrastructure.partnerportal.mq.model.PartnerPortalResponse

@Component
class PartnerPortalMapper {

    fun mapToDomain(response: PartnerPortalResponse, loanApplication: LoanApplication) =
        loanApplication.apply {
            response.comment?.let { comment ->
                addComment(Comment(text = comment))
            }
        }

    fun mapToRequest(loanApplication: LoanApplication) =
        PartnerPortalRequest(
            id = loanApplication.id.toString(),
            status = "init",
            comment = "value"
        )
}