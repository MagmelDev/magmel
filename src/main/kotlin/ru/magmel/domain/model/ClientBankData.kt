package ru.magmel.domain.model

import org.springframework.util.Assert
import java.util.*

class ClientBankData(
    val id: UUID = UUID.randomUUID(),
    val identifier: String? = null
) {

    init {
        identifier?.let {
            Assert.state(it.isNotBlank(), EMPTY_IDENTIFIER)
        }
    }

    companion object {
        const val EMPTY_IDENTIFIER = "EMPTY_IDENTIFIER"
    }
}