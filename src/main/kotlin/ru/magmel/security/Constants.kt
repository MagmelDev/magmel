package ru.magmel.security

class Constants {
    companion object {
        const val ROLE_PREFIX = "ROLE_"
        const val AUD_PREFIX = "AUD_"

        /** Audience **/
        const val AUDIENCE_MAGMEL = "magmel-api"

        /** Authorities **/
        const val AUTHORITIES_MAGMEL = AUD_PREFIX + AUDIENCE_MAGMEL

        /** Header **/
        const val X_USER_INFO_HEADER = "X-User-Info"
    }
}