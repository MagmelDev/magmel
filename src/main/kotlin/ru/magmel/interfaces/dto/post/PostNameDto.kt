package ru.magmel.interfaces.dto.post

import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

data class PostNameDto(
    @field:Pattern(regexp = "[А-ЯЁ][а-яё]+", message = "name.lastName:pattern")
    @field:Size(min = 1, max = 50, message = "name.lastName:size")
    val lastName: String,

    @field:Pattern(regexp = "[А-ЯЁ][а-яё]+", message = "name.firstName:pattern")
    @field:Size(min = 1, max = 50, message = "name.firstName:size")
    val firstName: String,

    @field:Pattern(regexp = "[А-ЯЁ][а-яё]+", message = "name.middleName:pattern")
    @field:Size(min = 0, max = 50, message = "name.middleName:size")
    val middleName: String?
)