package ru.magmel.application.canon

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.magmel.domain.ClientIdentifierSearcher
import ru.magmel.domain.model.Client
import ru.magmel.domain.repository.ClientRepository
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withId
import java.util.UUID

@Service
class ClientService(
    private val clientRepository: ClientRepository,
    private val clientIdentifierSearcher: ClientIdentifierSearcher,
    @Value("\${application.clientsearcher.attempts}") private val attempts: Int,
    @Value("\${application.clientsearcher.delay}") private val delay: Long,
    @Value("\${application.clientsearcher.timeout}") private val timeout: Long
) {

    private val log = LoggerFactory.getLogger(javaClass)

    @Transactional
    fun getAndSave(identifier: String, buildAction: (Client) -> Client) =
        clientRepository.get(identifier).also {
            log.info("Get {}", withId(it.id))
        }.apply {
            buildAction(this)
        }.let(clientRepository::save).also {
            log.info("Updated {}", withId(it.id))
        }

    fun searchClientIdentifiersByLoanApplicationId(id: UUID): List<Client> {
        log.info("Searching client identifiers {}", withBusinessKey(id))
        return clientRepository.findByLoanApplicationId(id).also { clients ->
            runBlocking {
                repeat(attempts) { attempt ->
                    val filteredClients = clients.filter { it.clientBankData == null }
                    if (filteredClients.isNotEmpty()) {
                        log.info("Attempt #$attempt {}", withBusinessKey(id))
                        searchClientIdentifiers(filteredClients)
                        if (attempt != attempts - 1) delay(delay)
                    }
                }
            }
        }
    }

    //todo Начать перезаписывать найденного клиента в заявке
    private fun searchClientIdentifiers(clients: List<Client>) =
        runBlocking {
            clients.map { client ->
                searchClientIdentifierAsync(client)
            }.awaitAll().filterNotNull().forEach { client ->
                client.clientBankData?.identifier?.let { identifier ->
                    clientRepository.find(identifier)?.also { client ->
                        log.info("Found client id=${client.id} identifier=${client.clientBankData}")
                    } ?: run {
                        log.info("Not found client id=${client.id} identifier=${client.clientBankData}")
                        clientRepository.save(client)
                    }
                }
            }
        }

    private fun searchClientIdentifierAsync(client: Client) =
        runBlocking {
            async(Dispatchers.Default) {
                log.info("Searching identifier id=${client.id}")
                runCatching {
                    client.findIdentifier(clientIdentifierSearcher).also {
                        log.info("Found identifier id=${client.id} identifier=${client.clientBankData}")
                    }
                }.getOrNull()
            }
        }
}