package ru.magmel.framework.aop

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.camunda.bpm.engine.impl.persistence.entity.ExecutionEntity
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import ru.magmel.util.withBusinessKey

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class DelegateAudition

@Aspect
@Component
class DelegateAuditionAspect {

    private val log = LoggerFactory.getLogger(javaClass)

    @Around("@annotation(ru.magmel.framework.aop.DelegateAudition)")
    fun delegateAudition(proceedingJoinPoint: ProceedingJoinPoint) {
        val businessKey = (proceedingJoinPoint.args.first() as ExecutionEntity).businessKey
        val className = proceedingJoinPoint.target.javaClass.name.split(".").last()
        log.trace("$className started {}", withBusinessKey(businessKey))
        proceedingJoinPoint.proceed()
        log.trace("$className finished {}", withBusinessKey(businessKey))
    }
}