package ru.magmel.interfaces.camunda.framework

import org.camunda.bpm.engine.delegate.DelegateExecution
import ru.magmel.domain.model.RiskDecision
import ru.magmel.domain.process.Variables.Companion.IDENTIFIER
import ru.magmel.domain.process.Variables.Companion.IDENTIFIERS
import ru.magmel.domain.process.Variables.Companion.RISK_DECISION
import java.util.*

fun DelegateExecution.identifier() = this.getVariable(IDENTIFIER) as String

@Suppress("UNCHECKED_CAST")
fun DelegateExecution.identifiers(): List<UUID> =
    (this.getVariable(IDENTIFIERS) as List<String>).map { UUID.fromString(it) }

fun DelegateExecution.riskDecision() = RiskDecision.valueOf(this.getVariable(RISK_DECISION) as String)