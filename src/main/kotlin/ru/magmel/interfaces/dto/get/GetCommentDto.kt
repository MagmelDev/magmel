package ru.magmel.interfaces.dto.get

data class GetCommentDto(
    val text: String
)
