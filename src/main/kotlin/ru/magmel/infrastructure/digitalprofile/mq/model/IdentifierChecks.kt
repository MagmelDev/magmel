package ru.magmel.infrastructure.digitalprofile.mq.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@XmlRootElement(name = "identifierChecks")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identifierChecks", propOrder = ["identifierChecks"])
data class IdentifierChecks(

    @field:XmlElement(name = "identifierCheck", required = true)
    var identifierChecks: List<IdentifierCheck>? = null
)