package ru.magmel.infrastructure.persistence.model

import ru.magmel.infrastructure.persistence.Schema
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "CREDIT_HISTORY", schema = Schema.CORE)
data class CreditHistory(
    @Id
    val id: UUID,
    val status: String? = null
)