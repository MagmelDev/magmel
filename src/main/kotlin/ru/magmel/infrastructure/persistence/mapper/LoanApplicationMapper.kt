package ru.magmel.infrastructure.persistence.mapper

import ru.magmel.domain.model.LoanApplication as DomainLoanApplication
import ru.magmel.domain.model.RiskDecision as DomainRiskDecision
import ru.magmel.infrastructure.persistence.model.ClientType
import ru.magmel.infrastructure.persistence.model.LoanApplication
import ru.magmel.infrastructure.persistence.model.RiskDecision

object LoanApplicationMapper {

    fun toDomain(loanApplication: LoanApplication): DomainLoanApplication {
        return DomainLoanApplication(
            id = loanApplication.id,
            borrowers = loanApplication.clients.filter { it.type == ClientType.BORROWER }
                .map(ClientMapper::toDomainBorrower).toMutableList(),
            pledgers = loanApplication.clients.filter { it.type == ClientType.PLEDGER }
                .map(ClientMapper::toDomainPledger).toMutableList(),
            comments = loanApplication.comments.map(CommentMapper::toDomain).toMutableList(),
            _riskDecision = loanApplication.riskDecision?.name?.let { DomainRiskDecision.valueOf(it) }
        )
    }

    fun toPersistence(domainLoanApplication: DomainLoanApplication): LoanApplication {
        return LoanApplication(
            id = domainLoanApplication.id,
            clients = (
                    domainLoanApplication.getBorrowers().map(ClientMapper::toPersistence) +
                            domainLoanApplication.getPledgers().map(ClientMapper::toPersistence)
                    ).toMutableList(),
            comments = domainLoanApplication.getComments().map(CommentMapper::toPersistence).toMutableList(),
            riskDecision = domainLoanApplication.riskDecision?.name?.let { RiskDecision.valueOf(it) }
        )
    }
}