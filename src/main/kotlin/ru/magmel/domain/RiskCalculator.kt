package ru.magmel.domain

import ru.magmel.domain.model.LoanApplication
import ru.magmel.domain.model.RiskDecision

interface RiskCalculator {
    fun calculateRisk(loanApplication: LoanApplication): Result<RiskDecision>
}