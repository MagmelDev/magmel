package ru.magmel.infrastructure.camunda.worker

import org.camunda.bpm.engine.ExternalTaskService
import org.camunda.bpm.engine.externaltask.LockedExternalTask
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.magmel.application.RiskService
import ru.magmel.domain.builder.VariablesBuilder
import ru.magmel.domain.model.RiskDecision
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withException
import ru.magmel.infrastructure.camunda.framework.businessKey
import ru.magmel.infrastructure.camunda.framework.CriticalWorker

/**
 * main.bpmn - Risk Calculation
 */
@Component
class RiskCalculationWorker(
    override val externalTaskService: ExternalTaskService,
    @Value("\${process.worker.riskCalculationWorker.initRetries}") override val initRetries: Int,
    @Value("\${process.worker.riskCalculationWorker.retryTimeout}") override val retryTimeout: Long,
    @Value("\${process.worker.riskCalculationWorker.lockDuration}") override val lockDuration: Long,
    @Value("\${process.worker.riskCalculationWorker.maxTasks}") override val maxTasks: Int,
    private val riskService: RiskService
) : CriticalWorker(topicName = "RiskCalculationTopicName", workerId = "RiskCalculationWorkerId") {

    override val log: Logger = LoggerFactory.getLogger(javaClass)

    @Scheduled(fixedDelay = POLLING_TIME)
    override fun scheduled() {
        super.fetchExternalTasks()
    }

    override fun execute(lockedExternalTask: LockedExternalTask) {
        runCatching {
            riskService.calculateRisk(lockedExternalTask.businessKey())
        }.onSuccess { riskDecision ->
            val variables = buildVariables(riskDecision)
            complete(lockedExternalTask, variables)
        }.onFailure { ex ->
            val errorMessage = "errorMessage"
            log.warn(
                "Unexpected error occurred {} {}",
                withBusinessKey(lockedExternalTask.businessKey),
                withException(ex)
            )
            handleCriticalFailure(
                lockedExternalTask,
                errorMessage = errorMessage,
                errorDetails = "errorDetails",
                errorCode = ERROR_CODE
            )
        }
    }

    private fun buildVariables(riskDecision: RiskDecision) =
        VariablesBuilder()
            .riskDecision(riskDecision)
            .please()

    companion object {
        private const val POLLING_TIME: Long = 1000
        private const val ERROR_CODE = "RiskCalculationError"
    }
}