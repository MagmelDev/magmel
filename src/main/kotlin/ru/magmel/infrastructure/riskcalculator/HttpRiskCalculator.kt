package ru.magmel.infrastructure.riskcalculator

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import ru.magmel.domain.RiskCalculator
import ru.magmel.domain.model.LoanApplication
import ru.magmel.domain.model.RiskDecision
import ru.magmel.util.withException
import ru.magmel.infrastructure.riskcalculator.model.Request
import ru.magmel.infrastructure.riskcalculator.model.Response

@Component
class HttpRiskCalculator(
    private val restTemplate: RestTemplate,
    @Value("\${integration.riskcalculator.host}") private val host: String
) : RiskCalculator {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun calculateRisk(loanApplication: LoanApplication): Result<RiskDecision> {
        return runCatching {
            val request = Request(loanApplication.getClients().mapNotNull { it.clientBankData?.identifier })
            val response = restTemplate.postForEntity("$host/calculate-risk", request, Response::class.java)
            mapRiskDecision(response.body!!.riskDecision)
        }.onSuccess { identifier ->
            log.info("Success! Found identifier=$identifier")
        }.onFailure { ex ->
            log.error("Unexpected error {}", withException(ex))
        }
    }

    fun mapRiskDecision(riskDecision: String): RiskDecision =
        when (riskDecision) {
            APPROVE -> RiskDecision.APPROVE
            REJECT -> RiskDecision.REJECT
            else -> throw IllegalArgumentException("Risk decision can not be mapper riskDecision=$riskDecision")
        }

    companion object {
        const val APPROVE = "approve"
        const val REJECT = "reject"
    }
}