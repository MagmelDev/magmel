package ru.magmel.interfaces.dto.get

import java.util.UUID

data class GetLoanApplicationResponse(
    val id: UUID,
    val borrowers: List<GetBorrowerDto> = listOf(),
    val comments: List<GetCommentDto> = listOf()
)