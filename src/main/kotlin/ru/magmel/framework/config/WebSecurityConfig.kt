package ru.magmel.framework.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.magmel.security.config.AuthorizeRequestsExtension
import ru.magmel.security.converters.UserInfoMagmelConverter

@Configuration
class WebSecurityConfig {

    @Bean
    fun userInfoMagmelConverter() = UserInfoMagmelConverter()

    @Bean
    @Throws(Exception::class)
    fun authorizeRequestsExtension() = AuthorizeRequestsExtension {
        it.apply {
            antMatchers(
                "/camunda/**",
                "/favicon.ico",
                "/actuator/**",
                "/stub/**",
                "/v3/api-docs/**",
                "/swagger-resources/**",
                "/swagger-ui/**",
                "/swagger-ui.html",
                "/private-api/**"
            ).permitAll()
        }
    }
}
