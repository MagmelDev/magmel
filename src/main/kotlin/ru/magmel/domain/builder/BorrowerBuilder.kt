package ru.magmel.domain.builder

import ru.magmel.domain.model.Borrower

class BorrowerBuilder {

    private val borrower = Borrower(
        name = NameHelper.vladimirMayakovsky()
    )

    fun withChanges(buildAction: (Borrower) -> Unit) = this.apply {
        buildAction(borrower)
    }

    fun please() = borrower
}