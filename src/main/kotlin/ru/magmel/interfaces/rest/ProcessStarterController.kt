package ru.magmel.interfaces.rest

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.magmel.domain.process.ProcessStarter
import ru.magmel.security.config.annotation.AuthorizeMagmel

@RestController
@RequestMapping("api/magmel/process/start")
class ProcessStarterController(
    private val processStarter: ProcessStarter
) {

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("main-process")
    @Operation(summary = "Start main process")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "OK"),
        ApiResponse(responseCode = "500", description = "Unexpected error")
    )
    @AuthorizeMagmel
    fun startMainProcess(businessKey: String): String =
        processStarter.startMainProcess(businessKey = businessKey)
}