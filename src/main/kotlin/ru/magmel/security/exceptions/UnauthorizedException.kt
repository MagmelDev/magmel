package ru.magmel.security.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

abstract class UnauthorizedException(reason: String) : ResponseStatusException(HttpStatus.UNAUTHORIZED, reason)
class UnauthorizedByEmptyUserInfo(reason: String) : UnauthorizedException(reason)