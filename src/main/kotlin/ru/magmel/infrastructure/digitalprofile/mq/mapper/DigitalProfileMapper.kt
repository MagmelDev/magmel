package ru.magmel.infrastructure.digitalprofile.mq.mapper

import org.springframework.stereotype.Component
import ru.magmel.domain.model.Comment
import ru.magmel.domain.model.LoanApplication
import ru.magmel.infrastructure.digitalprofile.mq.model.DigitalProfileRequest
import ru.magmel.infrastructure.digitalprofile.mq.model.DigitalProfileResponse

@Component
class DigitalProfileMapper {

    //todo Подумать куда стоит забирать эти данные
    fun mapToDomain(response: DigitalProfileResponse, loanApplication: LoanApplication) =
        loanApplication.apply {
            response.identifierChecks?.identifierChecks?.forEach { identifierCheck ->
                addComment(Comment(text = "${identifierCheck.identifier} ${identifierCheck.check}"))
            }
        }

    fun mapToRequest(loanApplication: LoanApplication) =
        DigitalProfileRequest(
            id = loanApplication.id.toString(),
            identifiers = loanApplication.getClients().mapNotNull { it.clientBankData?.identifier }
        )
}