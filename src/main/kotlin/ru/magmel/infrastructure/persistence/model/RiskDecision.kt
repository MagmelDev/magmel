package ru.magmel.infrastructure.persistence.model

enum class RiskDecision {
    APPROVE, REJECT
}