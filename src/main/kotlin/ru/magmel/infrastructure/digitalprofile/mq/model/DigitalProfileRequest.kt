package ru.magmel.infrastructure.digitalprofile.mq.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@XmlRootElement(name = "DigitalProfileRequest")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DigitalProfileRequest", propOrder = ["id", "identifiers"])
data class DigitalProfileRequest(

    @field:XmlElement(name = "id", required = true)
    var id: String? = null,

    @field:XmlElement(name = "identifiers", required = true)
    var identifiers: List<String>? = null
)