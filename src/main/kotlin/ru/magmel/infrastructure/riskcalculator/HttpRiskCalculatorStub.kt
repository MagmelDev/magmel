package ru.magmel.infrastructure.riskcalculator

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.magmel.framework.SpringProfile
import ru.magmel.infrastructure.riskcalculator.model.Request
import java.nio.charset.Charset
import java.util.UUID

@RestController
@RequestMapping("stub/risk-calculator")
@Profile(value = [SpringProfile.LOCAL])
class HttpRiskCalculatorStub {

    private val log = LoggerFactory.getLogger(javaClass)

    init {
        log.warn("${this.javaClass.name} stub is active!")
    }

    @PostMapping("calculate-risk")
    fun search(@RequestBody request: Request): ResponseEntity<String> {
        val body = resolveResponse(request)
        return ResponseEntity.ok(body)
    }

    private fun resolveResponse(request: Request): String {
        val uuid = UUID.randomUUID()
        log.info("Calculate risk fire uuid=$uuid, request=$request")
        return resource("/stub/riskcalculator/default-response.json").also {
            log.warn("Else case uuid=$uuid")
        }
    }

    private fun resource(fileName: String) = this.javaClass.getResource(fileName).readText(Charset.forName("UTF-8"))
}