package ru.magmel.security.config

import org.springframework.security.config.annotation.web.builders.HttpSecurity

@FunctionalInterface
fun interface HttpSecurityExtension {
    fun configure(httpSecurity: HttpSecurity)
}