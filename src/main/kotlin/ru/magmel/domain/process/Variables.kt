package ru.magmel.domain.process

class Variables {
    companion object {
        /**
         * List<String>
         */
        const val IDENTIFIERS = "identifiers"
        /**
         * String
         */
        const val IDENTIFIER = "identifier"
        /**
         * String (Enum)
         */
        const val RISK_DECISION = "riskDecision"
    }
}