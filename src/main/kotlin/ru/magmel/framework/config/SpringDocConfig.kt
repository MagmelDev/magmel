package ru.magmel.framework.config

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.security.SecurityScheme
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityRequirement
import org.springdoc.core.GroupedOpenApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import ru.magmel.framework.SpringProfile
import ru.magmel.security.Constants.Companion.X_USER_INFO_HEADER

@SecurityScheme(
    name = X_USER_INFO_HEADER,
    type = SecuritySchemeType.APIKEY,
    scheme = "bearer",
    bearerFormat = "JWT",
    `in` = SecuritySchemeIn.HEADER
)
@Configuration(proxyBeanMethods = false)
class SpringDocConfig {

    companion object {
        const val PUBLIC_API_VERSION = "1.0.0"
        const val PRIVATE_API_VERSION = "1.0.0"
    }

    @Bean
    @Profile("!${SpringProfile.PROD}")
    fun magmelPublicApi(): GroupedOpenApi? {
        return GroupedOpenApi.builder()
            .group("magmel-public-api")
            .pathsToMatch("/api/magmel/**")
            .displayName("magmel-public-api")
            .addOpenApiCustomiser {
                it.info(
                    Info()
                        .title("Magmel API documentation for related endpoints")
                        .version(PUBLIC_API_VERSION)
                ).addSecurityItem(SecurityRequirement().addList(X_USER_INFO_HEADER))
            }
            .build()
    }

    @Bean
    @Profile("!${SpringProfile.PROD}")
    fun magmelPrivateApi(): GroupedOpenApi? {
        return GroupedOpenApi.builder()
            .group("magmel-private-api")
            .pathsToMatch("/private-api/magmel/**")
            .displayName("magmel-private-api")
            .addOpenApiCustomiser {
                it.info(
                    Info()
                        .title("Magmel Private API documentation for related endpoints")
                        .version(PRIVATE_API_VERSION)
                )
            }
            .build()
    }
}
