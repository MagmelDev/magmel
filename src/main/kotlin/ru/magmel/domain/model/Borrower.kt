package ru.magmel.domain.model

import java.util.*

class Borrower : Client {

    constructor(name: Name) : super(name = name)

    constructor(id: UUID, name: Name, clientBankData: ClientBankData?, creditHistory: CreditHistory?) : super(id, name, clientBankData, creditHistory)
}