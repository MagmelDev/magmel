package ru.magmel.domain

import ru.magmel.domain.model.Name

interface ClientIdentifierSearcher {
    fun findByName(name: Name): Result<String?>
}