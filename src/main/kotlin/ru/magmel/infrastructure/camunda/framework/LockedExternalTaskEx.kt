package ru.magmel.infrastructure.camunda.framework

import org.camunda.bpm.engine.externaltask.LockedExternalTask
import java.util.*

fun LockedExternalTask.businessKey(): UUID = UUID.fromString(this.businessKey)