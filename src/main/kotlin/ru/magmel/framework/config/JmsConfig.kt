package ru.magmel.framework.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jms.annotation.EnableJms
import org.springframework.jms.config.DefaultJmsListenerContainerFactory
import org.springframework.jms.config.JmsListenerContainerFactory
import org.springframework.jms.core.JmsTemplate
import ru.magmel.framework.SpringProfile
import ru.magmel.framework.properties.MqManagerProperties
import javax.jms.ConnectionFactory
import javax.jms.JMSException

@Configuration
@EnableJms
@Profile(value = [SpringProfile.TEST, SpringProfile.PREVIEW, SpringProfile.PROD])
class JmsConfig : JmsAbstractConfig {

    @Bean
    @Throws(JMSException::class)
    fun jmsConnectionFactory(mqManagerProperties: MqManagerProperties): ConnectionFactory =
        createConnectionFactory(mqManagerProperties)

    @Bean
    fun jmsTemplate(
        jmsConnectionFactory: ConnectionFactory,
        mqManagerProperties: MqManagerProperties
    ): JmsTemplate =
        createJmsTemplate(jmsConnectionFactory, mqManagerProperties.timeout)

    @Bean
    fun jmsListenerContainerFactory(
        jmsConnectionFactory: ConnectionFactory
    ): JmsListenerContainerFactory<*> =
        DefaultJmsListenerContainerFactory().apply {
            setConnectionFactory(jmsConnectionFactory)
            setPubSubDomain(java.lang.Boolean.FALSE)
        }
}