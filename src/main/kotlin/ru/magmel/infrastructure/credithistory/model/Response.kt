package ru.magmel.infrastructure.credithistory.model

data class Response(
    val client: Client
) {
    data class Client(
        val status: String?
    )
}