package ru.magmel.infrastructure.digitalprofile.mq

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.jms.core.JmsTemplate
import org.springframework.stereotype.Component
import ru.magmel.domain.DigitalProfileSender
import ru.magmel.domain.model.LoanApplication
import ru.magmel.infrastructure.digitalprofile.mq.mapper.DigitalProfileMapper
import ru.magmel.infrastructure.digitalprofile.mq.model.DigitalProfileRequest
import ru.magmel.infrastructure.framework.mq.AbstractJmsSender
import ru.magmel.util.withJmsMessageId
import java.util.concurrent.atomic.AtomicReference
import javax.jms.TextMessage

@Component
class DigitalProfileJmsSender(
    private val digitalProfileMapper: DigitalProfileMapper,
    private val jmsTemplate: JmsTemplate,
    @Value("\${integration.digitalProfile.requestQueue}") private val requestQueue: String,
    @Value("\${integration.digitalProfile.responseQueue}") private val responseQueue: String
) : DigitalProfileSender, AbstractJmsSender() {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun send(loanApplication: LoanApplication): String {
        return loanApplication
            .let(digitalProfileMapper::mapToRequest)
            .let(::send)
    }

    private fun send(request: DigitalProfileRequest): String {
        val requestMessage = parseSoapMessage<DigitalProfileRequest>(request).also { requestMessage ->
            log.info("RequestMessage {}", requestMessage)
        }
        return send(requestMessage).also { jmsMessageID ->
            log.info("Send {} {}", request.id, withJmsMessageId(jmsMessageID))
        }
    }

    private fun send(requestMessage: String): String {
        val messageReference = AtomicReference<TextMessage>()
        jmsTemplate.send(requestQueue) { session ->
            val textMessage = session.createTextMessage(requestMessage)
            textMessage.jmsReplyTo = session.createQueue(responseQueue)
            messageReference.set(textMessage)
            messageReference.get()
        }
        return messageReference.get().jmsMessageID
    }
}