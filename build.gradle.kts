import io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension

val camundaVersion = "7.19.0"
val apacheHttpClient = "4.5.13"
val kotlinVersion = "1.5.0"
val springBomVersion = "1.0.2"

plugins {
    val kotlinVersion = "1.5.0"
    val springBootVersion = "2.7.0"
    val springDependencyManagementVersion = "1.0.11.RELEASE"

    kotlin("jvm") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
    kotlin("plugin.jpa") version kotlinVersion
    kotlin("plugin.allopen") version kotlinVersion

    id("org.springframework.boot") version springBootVersion
    id("io.spring.dependency-management") version springDependencyManagementVersion

    groovy
}

repositories {
    mavenCentral()
}

configure<DependencyManagementExtension> {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:2021.0.2")
        mavenBom("org.springframework.boot:spring-boot-dependencies:2.7.0")
        mavenBom("org.camunda.bpm:camunda-bom:$camundaVersion")
    }
}

dependencies {
    /**
     * JAVA EE
     */
    implementation(platform("com.sun.xml.ws:jaxws-ri:2.3.3"))
    implementation("com.sun.xml.ws:jaxws-rt")
    implementation("com.sun.xml.bind:jaxb-impl:2.3.3")
    implementation("javax.annotation:javax.annotation-api:1.3.2")
    /**
     * Kotlin
     */
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$kotlinVersion")
    /**
     * Spring
     */
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-data-rest")
    implementation("org.springframework.boot:spring-boot-starter-activemq")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.boot:spring-boot-starter-security")
    /**
     * Database
     */
    implementation("org.testcontainers:testcontainers:1.16.2")
    implementation("org.testcontainers:postgresql:1.16.2")
    /**
     * Postgresql
     */
    runtimeOnly("org.postgresql:postgresql:42.3.1")
    /**
     * Groovy
     */
    implementation("org.codehaus.groovy:groovy-all:2.4.15")
    /**
     * Logging
     */
    implementation("ch.qos.logback:logback-core:1.2.3")
    implementation("net.logstash.logback:logstash-logback-encoder:6.6")
    implementation("com.google.guava:guava:30.1.1-jre")
    /**
     * Migration
     */
    implementation("org.liquibase:liquibase-core")
    /**
     * Json
     */
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    /**
     * Open API
     */
    implementation("org.springdoc:springdoc-openapi-data-rest:1.6.13")
    implementation("org.springdoc:springdoc-openapi-ui:1.6.13")
    implementation("org.springdoc:springdoc-openapi-kotlin:1.6.13")
    /**
     * Actuator
     */
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    runtimeOnly("io.micrometer:micrometer-registry-prometheus")
    /**
     * Mq
     */
    implementation("com.ibm.mq:com.ibm.mq.allclient:9.2.2.0")
    implementation("com.migesok:jaxb-java-time-adapters:1.1.3")
    /**
     * Camunda
     */
    implementation("org.camunda.bpm.springboot:camunda-bpm-spring-boot-starter-webapp:$camundaVersion")
    /**
     * Http
     */
    implementation("org.apache.httpcomponents:httpclient:$apacheHttpClient")
    /**
     * Gson
     */
    implementation("com.google.code.gson:gson:2.8.7")
    /**
     * Test
     */
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.spockframework:spock-spring:1.3-groovy-2.4")
    testImplementation("com.nimbusds:oauth2-oidc-sdk:10.8")
}

allOpen {
    annotation("ru.magmel.framework.AllOpen")
}

tasks.compileKotlin {
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs = listOf("-Xjsr305=strict")
        allWarningsAsErrors = true
    }
}

tasks.compileTestKotlin {
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs = listOf("-Xjsr305=strict")
    }
}

tasks.compileTestGroovy {
    val kotlinCompile = tasks.compileTestKotlin.get()
    dependsOn(kotlinCompile)
    classpath += files(kotlinCompile.destinationDirectory)
}
