package ru.magmel.infrastructure.camunda

import org.camunda.bpm.engine.RuntimeService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import ru.magmel.domain.process.ProcessStarter
import ru.magmel.util.withBusinessKey
import ru.magmel.util.withProcessInstanceId
import ru.magmel.infrastructure.camunda.CamundaProcessStarter.MessageName.Companion.START_MAIN_PROCESS

@Component
class CamundaProcessStarter(
    private val runtimeService: RuntimeService
) : ProcessStarter {

    private val log = LoggerFactory.getLogger(javaClass)

    override fun startMainProcess(businessKey: String, variables: Map<String, Any>): String {
        log.info("Starting main process {}", withBusinessKey(businessKey))
        return runtimeService
            .createMessageCorrelation(START_MAIN_PROCESS)
            .processInstanceBusinessKey(businessKey)
            .setVariables(variables)
            .correlateStartMessage().id.also { processInstanceId ->
                log.info(
                    "Main process started {} {}",
                    withBusinessKey(businessKey),
                    withProcessInstanceId(processInstanceId)
                )
            }
    }

    class MessageName {
        companion object {
            const val START_MAIN_PROCESS = "Message_StartMainProcess"
        }
    }
}
