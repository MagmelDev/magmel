package ru.magmel.interfaces.rest

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.context.annotation.Profile
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.domain.builder.NameHelper
import ru.magmel.domain.builder.ObjectMother
import ru.magmel.domain.model.LoanApplication
import ru.magmel.domain.process.ProcessStarter
import ru.magmel.framework.SpringProfile
import ru.magmel.security.config.annotation.AuthorizeMagmel
import java.util.UUID

@RestController
@RequestMapping("api/magmel/smoke-test")
@Profile(value = [SpringProfile.LOCAL])
class SmokeTestController(
    private val loanApplicationService: LoanApplicationService,
    private val processStarter: ProcessStarter
) {

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("approve-flow")
    @Operation(description = "Approve flow")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "OK"),
        ApiResponse(responseCode = "500", description = "Unexpected error")
    )
    @AuthorizeMagmel
    fun approveFlow(): String {
        val loanApplication = LoanApplication(
            id = UUID.randomUUID(),
            borrowers = mutableListOf(
                ObjectMother.borrower()
                    .withChanges {
                        it.name = NameHelper.vladimirMayakovsky()
                    }
                    .please(),
                ObjectMother.borrower()
                    .withChanges {
                        it.name = NameHelper.andrzejSapkowski()
                    }
                    .please()
            ),
            pledgers = mutableListOf(
                ObjectMother.pledger()
                    .withChanges {
                        it.name = NameHelper.mikhailBulgakov()
                    }
                    .please(),
                ObjectMother.pledger()
                    .withChanges {
                        it.name = NameHelper.charlesMaclean()
                    }
                    .please()
            )
        )
        val id = loanApplicationService.create(loanApplication).id
        processStarter.startMainProcess(businessKey = id.toString())
        return id.toString()
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping("approve-flow-without-risk-calculation")
    @Operation(description = "Approve flow without risk calculation")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "OK"),
        ApiResponse(responseCode = "500", description = "Unexpected error")
    )
    @AuthorizeMagmel
    fun approveFlowWithoutRiskCalculation(): String {
        val loanApplication = LoanApplication(
            id = UUID.randomUUID(),
            borrowers = mutableListOf(
                ObjectMother.borrower()
                    .withChanges {
                        it.name = NameHelper.andrzejSapkowski()
                    }
                    .please()
            ),
            pledgers = mutableListOf(
                ObjectMother.pledger()
                    .withChanges {
                        it.name = NameHelper.charlesMaclean()
                    }
                    .please()
            )
        )
        val id = loanApplicationService.create(loanApplication).id
        processStarter.startMainProcess(businessKey = id.toString())
        return id.toString()
    }
}