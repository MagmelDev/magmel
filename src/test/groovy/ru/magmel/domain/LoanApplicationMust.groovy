package ru.magmel.domain

import ru.magmel.domain.builder.ObjectMother
import ru.magmel.domain.model.LoanApplication
import spock.lang.Specification

class LoanApplicationMust extends Specification {

    def create = ObjectMother.INSTANCE

    def "not throw exception when there are borrowers amount <= 2 , pledgers amount <= 3  and clients amount <= 4"() {
        when:
        def loanApplication = new LoanApplication(
                UUID.randomUUID(),
                [create.borrower().please(), create.borrower().please()],
                [create.pledger().please(), create.pledger().please()],
                [],
                null
        )

        then:
        noExceptionThrown()
        loanApplication != null

        when:
        loanApplication = new LoanApplication(
                UUID.randomUUID(),
                [create.borrower().please()],
                [create.pledger().please(), create.pledger().please(), create.pledger().please()],
                [],
                null
        )

        then:
        noExceptionThrown()
        loanApplication != null
    }

    def "throw IllegalStateException when there are no borrowers"() {
        when:
        new LoanApplication(
                UUID.randomUUID(),
                [],
                [],
                [],
                null
        )

        then:
        def ex = thrown(IllegalStateException)
        ex.message == LoanApplication.MIN_BORROWERS_SIZE_MESSAGE
    }

    def "throw IllegalStateException when there are more than 2 borrowers"() {
        when:
        new LoanApplication(
                UUID.randomUUID(),
                [create.borrower().please(), create.borrower().please(), create.borrower().please()],
                [],
                [],
                null
        )

        then:
        def ex = thrown(IllegalStateException)
        ex.message == LoanApplication.MAX_BORROWERS_SIZE_MESSAGE
    }

    def "throw IllegalStateException when there are more than 2 borrowers after adding"() {
        when:
        def loanApplication = new LoanApplication(
                UUID.randomUUID(),
                [create.borrower().please(), create.borrower().please()],
                [],
                [],
                null
        )
        loanApplication.addBorrower(create.borrower().please())

        then:
        def ex = thrown(IllegalStateException)
        ex.message == LoanApplication.MAX_BORROWERS_SIZE_MESSAGE
    }

    def "throw IllegalStateException when there are more than 4 client"() {
        when:
        new LoanApplication(
                UUID.randomUUID(),
                [
                        create.borrower().please(),
                        create.borrower().please()
                ],
                [
                        create.pledger().please(),
                        create.pledger().please(),
                        create.pledger().please()
                ],
                [],
                null
        )

        then:
        def ex = thrown(IllegalStateException)
        ex.message == LoanApplication.MAX_CLIENTS_SIZE_MESSAGE
    }

    def "throw IllegalStateException when there are more than 4 client after adding"() {
        when:
        def loanApplication = new LoanApplication(
                UUID.randomUUID(),
                [
                        create.borrower().please()
                ],
                [
                        create.pledger().please(),
                        create.pledger().please(),
                        create.pledger().please()
                ],
                [],
                null
        )
        loanApplication.addBorrower(create.borrower().please())

        then:
        def ex = thrown(IllegalStateException)
        ex.message == LoanApplication.MAX_CLIENTS_SIZE_MESSAGE

        when:
        loanApplication = new LoanApplication(
                UUID.randomUUID(),
                [
                        create.borrower().please(),
                        create.borrower().please()
                ],
                [
                        create.pledger().please(),
                        create.pledger().please(),
                        create.pledger().please()
                ],
                [],
                null
        )
        loanApplication.addPledger(create.pledger().please())

        then:
        ex = thrown(IllegalStateException)
        ex.message == LoanApplication.MAX_CLIENTS_SIZE_MESSAGE
    }
}
