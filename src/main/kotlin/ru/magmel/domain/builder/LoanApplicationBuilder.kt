package ru.magmel.domain.builder

import ru.magmel.domain.model.LoanApplication
import java.util.*

class LoanApplicationBuilder {

    private val loanApplication = LoanApplication(
        id = UUID.randomUUID(),
        borrowers = mutableListOf(ObjectMother.borrower().please())
    )

    fun please() = loanApplication
}