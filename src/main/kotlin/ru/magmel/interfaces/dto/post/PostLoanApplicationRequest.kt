package ru.magmel.interfaces.dto.post

import javax.validation.Valid

data class PostLoanApplicationRequest(
    @field:Valid
    val borrowers: List<PostBorrowerDto> = listOf()
)