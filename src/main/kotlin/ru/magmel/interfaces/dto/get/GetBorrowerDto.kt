package ru.magmel.interfaces.dto.get

data class GetBorrowerDto(
    val name: GetNameDto
)
