package ru.magmel.security.converters

import org.json.JSONArray
import org.json.JSONObject
import java.util.UUID

class UserInfoConverterUtils {
    companion object {
        fun canAppliedByAudience(userInfo: JSONObject, audience: String): Boolean {
            val aud = userInfo.opt("aud") ?: return false
            if (aud is String) {
                return aud == audience
            }
            if (aud is JSONArray) {
                return aud.contains(audience)
            }
            return false
        }
        fun getClientId(userInfo: JSONObject): String = userInfo.getString("client_id")
        fun getUserName(userInfo: JSONObject): String = userInfo.getString("preferred_username")
    }
}