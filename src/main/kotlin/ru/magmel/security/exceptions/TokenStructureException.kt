package ru.magmel.security.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

abstract class TokenStructureException(reason: String) : ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, reason)

class NoPartnerBonusTypeException : TokenStructureException("No partner bonus type")

class NoRoleException : TokenStructureException("No role")
class MultipleRoleException(roles: List<*>) : TokenStructureException("Multiple role: $roles")
class UnsupportedRoleException(role: String?) : TokenStructureException("Unsupported role: $role")

class NoPartnerRdmIdException : TokenStructureException("No partner RDM ID")
class NoHoldingRdmIdException : TokenStructureException("No holding RDM ID")