package ru.magmel.domain.model

import org.springframework.util.Assert
import java.util.*

class Comment(
    val id: UUID = UUID.randomUUID(),
    val text: String
) {
    init {
        Assert.state(text.isNotBlank(), EMPTY_TEXT)
    }

    companion object {
        const val EMPTY_TEXT = "EMPTY_TEXT"
    }
}