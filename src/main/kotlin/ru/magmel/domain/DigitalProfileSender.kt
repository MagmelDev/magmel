package ru.magmel.domain

import ru.magmel.domain.model.LoanApplication

interface DigitalProfileSender {
    fun send(loanApplication: LoanApplication): String
}