package ru.magmel.security.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

abstract class AccessDeniedException(reason: String? = null) : ResponseStatusException(HttpStatus.FORBIDDEN, reason)

class AccessDeniedByResourceAccessNotFoundException : AccessDeniedException("Access denied by resource access not found")
class AccessDeniedByResourceAccessByAudienceNotFoundException(audience: String) : AccessDeniedException("Access denied by resource access for audience = $audience")
