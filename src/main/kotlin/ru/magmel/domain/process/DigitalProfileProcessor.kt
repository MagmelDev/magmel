package ru.magmel.domain.process

interface DigitalProfileProcessor {
    fun process(businessKey: String, variables: Map<String, Any> = emptyMap()): String
}