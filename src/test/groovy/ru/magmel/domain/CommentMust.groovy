package ru.magmel.domain

import ru.magmel.domain.model.Comment
import spock.lang.Specification

class CommentMust extends Specification {

    def "not throw exception"() {
        when:
        def comment = new Comment(UUID.randomUUID(), "Обычный комментарий")

        then:
        noExceptionThrown()
        comment != null
    }

    def "throw exception"() {
        when:
        new Comment(UUID.randomUUID(), text)

        then:
        def ex = thrown(IllegalStateException)
        ex.message == Comment.EMPTY_TEXT

        where:
        text << ["", " ", "  "]
    }
}
