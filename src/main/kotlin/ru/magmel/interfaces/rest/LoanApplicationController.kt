package ru.magmel.interfaces.rest

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.interfaces.assembler.LoanApplicationAssembler
import ru.magmel.interfaces.dto.get.GetLoanApplicationResponse
import ru.magmel.interfaces.dto.post.PostLoanApplicationRequest
import ru.magmel.security.config.annotation.AuthorizeMagmel
import java.util.UUID
import javax.validation.Valid

@RestController
@RequestMapping("api/magmel/loan-application")
class LoanApplicationController(
    private val loanApplicationService: LoanApplicationService
) {

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    @Operation(summary = "Create loan application")
    @ApiResponses(
        ApiResponse(responseCode = "201", description = "Created"),
        ApiResponse(responseCode = "500", description = "Unexpected error")
    )
    @AuthorizeMagmel
    fun createLoanApplication(@Valid @RequestBody request: PostLoanApplicationRequest): String {
        val id = loanApplicationService.create(LoanApplicationAssembler.toDomain(request)).id
        return id.toString()
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get loan application")
    @ApiResponses(
        ApiResponse(
            responseCode = "200",
            description = "OK",
            content = [Content(
                mediaType = "application/json",
                schema = Schema(implementation = GetLoanApplicationResponse::class)
            )]
        ),
        ApiResponse(responseCode = "500", description = "Unexpected error", content = [Content()])
    )
    @AuthorizeMagmel
    fun getLoanApplication(@PathVariable id: UUID): ResponseEntity<GetLoanApplicationResponse> {
        return loanApplicationService.find(id)?.let { loanApplication ->
            ResponseEntity.ok(LoanApplicationAssembler.toDto(loanApplication))
        } ?: ResponseEntity.notFound().build()
    }
}