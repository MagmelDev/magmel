package ru.magmel.infrastructure.digitalprofile.mq.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlType

@XmlRootElement(name = "identifierCheck")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identifierCheck", propOrder = ["identifier", "check"])
data class IdentifierCheck(

    @field:XmlElement(name = "identifier", required = true)
    var identifier: String? = null,

    @field:XmlElement(name = "check", required = true)
    var check: Boolean? = null
)