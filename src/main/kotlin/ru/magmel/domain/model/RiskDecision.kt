package ru.magmel.domain.model

enum class RiskDecision {
    APPROVE, REJECT
}