package ru.magmel.infrastructure.partnerportal.mq

import org.slf4j.LoggerFactory
import org.springframework.jms.annotation.JmsListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import ru.magmel.application.canon.LoanApplicationService
import ru.magmel.domain.PartnerPortalListener
import ru.magmel.domain.model.LoanApplication
import ru.magmel.domain.process.PartnerPortalProcessor
import ru.magmel.infrastructure.framework.mq.AbstractJmsListener
import ru.magmel.infrastructure.partnerportal.mq.mapper.PartnerPortalMapper
import ru.magmel.infrastructure.partnerportal.mq.model.PartnerPortalResponse
import ru.magmel.util.withException
import ru.magmel.util.withJmsMessageId
import java.util.UUID
import javax.jms.Message

@Component
class PartnerPortalJmsListener(
    private val loanApplicationService: LoanApplicationService,
    private val partnerPortalMapper: PartnerPortalMapper,
    private val partnerPortalProcessor: PartnerPortalProcessor
) : PartnerPortalListener, AbstractJmsListener() {

    private val log = LoggerFactory.getLogger(javaClass)

    @JmsListener(
        destination = "\${integration.partnerPortal.responseQueue}",
        containerFactory = "jmsListenerContainerFactory"
    )
    override fun onMessage(@Payload message: Message) {
        runCatching {
            val jmsMessageId = message.jmsMessageID
            val jmsCorrelationId = message.jmsCorrelationID
            log.info("Receive {} {}", withJmsMessageId(jmsMessageId), jmsCorrelationId)
            val response = getMessagePayload(message).also { messagePayload ->
                log.info("MessagePayload {} {} {}", messagePayload, withJmsMessageId(jmsMessageId), jmsCorrelationId)
            }
            processResponse(response).also {
                log.info("Response {} {} {}", response, withJmsMessageId(jmsMessageId), jmsCorrelationId)
            }
        }.onFailure { ex ->
            log.error("{}", withException(ex))
        }
    }

    override fun processResponse(response: String) {
        val partnerPortalResponse = extractResponse<PartnerPortalResponse>(response)
        loanApplicationService.getAndSave(UUID.fromString(partnerPortalResponse.id)) { loanApplication ->
            partnerPortalMapper.mapToDomain(partnerPortalResponse, loanApplication)
        }.let(::receive)
    }

    override fun receive(loanApplication: LoanApplication) {
        partnerPortalProcessor.process(loanApplication.id.toString())
    }
}