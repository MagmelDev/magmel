package ru.magmel.security.config

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.Authentication
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter
import ru.magmel.security.config.filter.CurrentUserAuthenticationFilter
import ru.magmel.security.converters.UserInfoConverter

@EnableWebSecurity
@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Import(MethodSecurityConfiguration::class)
class ServiceSecurityAutoConfiguration(
    private val converters: List<UserInfoConverter<out Authentication>>,
    private val authorizeRequestsExtension: AuthorizeRequestsExtension?,
    private val httpSecurityExtension: List<HttpSecurityExtension>?
) {
    @Bean
    @Throws(Exception::class)
    fun filterChain(http: HttpSecurity): SecurityFilterChain {
        http.csrf().disable()
            .httpBasic().disable()
            .formLogin().disable()
            .logout().disable()
        http.authorizeRequests().let {
            (authorizeRequestsExtension?.configure(it) ?: it).anyRequest().authenticated()
        }
        http
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .sessionAuthenticationStrategy(NullAuthenticatedSessionStrategy())
            .and()
            .securityContext()
            .requireExplicitSave(true)
            .securityContextRepository(CustomRequestAttributeSecurityContextRepository())
            .and()
            .addFilterAfter(
                CurrentUserAuthenticationFilter(converters),
                SecurityContextHolderAwareRequestFilter::class.java
            )
        httpSecurityExtension?.forEach { it.configure(http) }
        return http.build()
    }
}