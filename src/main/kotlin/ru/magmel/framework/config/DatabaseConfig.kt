package ru.magmel.framework.config

import com.zaxxer.hikari.HikariDataSource
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import javax.sql.DataSource

@Configuration
class DatabaseConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    @Primary
    fun dataSource(): DataSource = HikariDataSource()

    @Bean
    @ConfigurationProperties(prefix = "camunda.datasource")
    fun camundaBpmDataSource(): DataSource = HikariDataSource()
}