package ru.magmel.domain.model

import org.springframework.util.Assert

data class Name(
    val lastName: String,
    val firstName: String,
    val middleName: String?
) {
    init {
        Assert.state(lastName.isNotBlank(), EMPTY_LAST_NAME)
        Assert.state(lastName.matches(Regex(REGEX)), INCORRECT_LAST_NAME)
        Assert.state(firstName.isNotBlank(), EMPTY_FIRST_NAME)
        Assert.state(firstName.matches(Regex(REGEX)), INCORRECT_FIRST_NAME)
        middleName?.let {
            Assert.state(it.isNotBlank(), EMPTY_MIDDLE_NAME)
            Assert.state(it.matches(Regex(REGEX)), INCORRECT_MIDDLE_NAME)
        }
    }

    val fullName = "$lastName $firstName ${middleName.orEmpty()}".trim()

    companion object {
        const val REGEX = "^[а-яА-ЯёЁ]*\$"
        const val EMPTY_LAST_NAME = "EMPTY_LAST_NAME"
        const val INCORRECT_LAST_NAME = "INCORRECT_LAST_NAME"
        const val EMPTY_FIRST_NAME = "EMPTY_FIRST_NAME"
        const val INCORRECT_FIRST_NAME = "INCORRECT_FIRST_NAME"
        const val EMPTY_MIDDLE_NAME = "EMPTY_MIDDLE_NAME"
        const val INCORRECT_MIDDLE_NAME = "INCORRECT_MIDDLE_NAME"
    }
}