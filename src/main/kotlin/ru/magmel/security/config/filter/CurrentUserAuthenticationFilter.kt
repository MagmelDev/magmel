package ru.magmel.security.config.filter

import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import ru.magmel.util.withException
import ru.magmel.security.Constants
import ru.magmel.security.converters.UserInfoConverter
import java.lang.invoke.MethodHandles
import java.util.Base64
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

@Order(Ordered.LOWEST_PRECEDENCE - 4)
class CurrentUserAuthenticationFilter(
    private val converters: List<UserInfoConverter<out Authentication>>
) : GenericFilterBean() {

    companion object {
        private val log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())
    }

    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        var currentUser: Authentication? = null
        val httpServletRequest = request as HttpServletRequest
        val userInfoBase64 = httpServletRequest.getHeader(Constants.X_USER_INFO_HEADER)
        if (userInfoBase64 != null) {
            JSONObject(Base64.getDecoder().decode(userInfoBase64).decodeToString()).let { json ->
                currentUser = converters.firstOrNull {
                    it.runCatching {
                        canApplied(json)
                    }.getOrElse { e ->
                        log.error("Converter's canApplied condition throws exception, {}", withException(e))
                        false
                    }
                }?.runCatching {
                    decodeJson(json)
                }?.getOrElse { e ->
                    log.error("Decode json throws exception, {}", withException(e))
                    null
                }
            }
        }
        SecurityContextHolder.getContext().authentication = currentUser
        chain?.doFilter(request, response)
    }
}
