package ru.magmel.interfaces.camunda.delegate

import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.springframework.stereotype.Component
import ru.magmel.application.PartnerPortalService
import ru.magmel.framework.aop.DelegateAudition
import java.util.*

@Component("SendMessageToPartnerPortalDelegate")
class SendMessageToPartnerPortalDelegate(
    private val partnerPortalService: PartnerPortalService
) : JavaDelegate {

    @DelegateAudition
    override fun execute(execution: DelegateExecution) {
        val businessKey = execution.businessKey
        partnerPortalService.send(UUID.fromString(businessKey))
    }
}