package ru.magmel.interfaces.assembler

import ru.magmel.domain.model.Comment
import ru.magmel.interfaces.dto.get.GetCommentDto

object CommentAssembler {

    fun toDto(comment: Comment): GetCommentDto =
        GetCommentDto(
            text = comment.text
        )
}