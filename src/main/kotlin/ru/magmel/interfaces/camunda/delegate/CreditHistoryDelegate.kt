package ru.magmel.interfaces.camunda.delegate

import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.springframework.stereotype.Component
import ru.magmel.application.CreditHistoryService
import ru.magmel.framework.aop.DelegateAudition
import ru.magmel.interfaces.camunda.framework.identifier

@Component("CreditHistoryDelegate")
class CreditHistoryDelegate(
    private val creditHistoryService: CreditHistoryService
) : JavaDelegate {

    @DelegateAudition
    override fun execute(execution: DelegateExecution) {
        val identifier = execution.identifier()
        creditHistoryService.get(identifier)
    }
}