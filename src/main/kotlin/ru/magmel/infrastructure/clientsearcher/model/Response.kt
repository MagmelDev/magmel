package ru.magmel.infrastructure.clientsearcher.model

data class Response(
    val client: Client
) {
    data class Client(
        val identifier: String?
    )
}