package ru.magmel.domain.model

import org.springframework.util.Assert
import ru.magmel.domain.RiskCalculator
import java.util.*

class LoanApplication(

    val id: UUID,

    private val borrowers: MutableList<Borrower> = mutableListOf(),

    private val pledgers: MutableList<Pledger> = mutableListOf(),

    private val comments: MutableList<Comment> = mutableListOf(),

    private var _riskDecision: RiskDecision? = null
) {

    init {
        Assert.state(borrowers.size >= MIN_BORROWERS_SIZE, MIN_BORROWERS_SIZE_MESSAGE)
        Assert.state(borrowers.size + pledgers.size <= MAX_CLIENTS_SIZE, MAX_CLIENTS_SIZE_MESSAGE)
        Assert.state(borrowers.size <= MAX_BORROWERS_SIZE, MAX_BORROWERS_SIZE_MESSAGE)
        Assert.state(borrowers.size <= MAX_BORROWERS_SIZE, MAX_BORROWERS_SIZE_MESSAGE)
    }

    fun getBorrowers() = this.borrowers.toList()

    fun addBorrower(borrower: Borrower) = this.apply {
        Assert.state(borrowers.size + pledgers.size < MAX_CLIENTS_SIZE, MAX_CLIENTS_SIZE_MESSAGE)
        Assert.state(borrowers.size < MAX_BORROWERS_SIZE, MAX_BORROWERS_SIZE_MESSAGE)
        borrowers.add(borrower)
    }

    fun getPledgers() = this.pledgers.toList()

    fun addPledger(pledger: Pledger) = this.apply {
        Assert.state(borrowers.size + pledgers.size < MAX_CLIENTS_SIZE, MAX_CLIENTS_SIZE_MESSAGE)
        pledgers.add(pledger)
    }

    fun getClients() = this.borrowers as List<Client> + this.pledgers as List<Client>

    fun getComments() = this.comments.toList()

    fun addComment(comment: Comment) = this.apply {
        comments.add(comment)
    }

    fun calculateRisk(riskCalculator: RiskCalculator) =
        this.apply {
            Assert.state(
                getClients().mapNotNull { it.clientBankData?.identifier }.isNotEmpty(),
                CALCULATE_RISK_WITHOUT_IDENTIFIERS
            )
            riskCalculator.calculateRisk(this)
                .onSuccess { riskDecision ->
                    this._riskDecision = riskDecision
                }.getOrThrow()
        }

    val riskDecision: RiskDecision?
        get() {
            return if (_riskDecision == null) {
                null
            } else _riskDecision ?: throw AssertionError("Set to null by another thread")
        }

    companion object {
        const val MIN_BORROWERS_SIZE_MESSAGE = "MIN_BORROWER_SIZE"
        const val MAX_BORROWERS_SIZE_MESSAGE = "MAX_BORROWERS_SIZE"
        const val MAX_CLIENTS_SIZE_MESSAGE = "CLIENT_SIZE"
        const val CALCULATE_RISK_WITHOUT_IDENTIFIERS = "CALCULATE_RISK_WITHOUT_IDENTIFIERS"

        private const val MIN_BORROWERS_SIZE = 1
        private const val MAX_BORROWERS_SIZE = 2
        private const val MAX_CLIENTS_SIZE = 4
    }
}
